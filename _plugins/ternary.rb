module Jekyll
    module TernaryFilters
      def default(value, default)
        value.nil? ? default : value
      end
    end
  end