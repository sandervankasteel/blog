---
layout: page
title: Projects
permalink: /projects/
background: '/images/bg-projects.jpg'
---

Below you will find some (but not all) of my side-projects. Some actively maintained, some are forgotten, some are opensource and some will be closed source. In case they are open source, check out the source and let me know what you think of it. 

{::options parse_block_html="false" /}

{% for project in site.projects %}

  {% assign mod = forloop.index0 | modulo:2 %}

  {% if forloop.first or mod == 0%}
  <div class="card-deck">
  {% endif %}

  <div class="card">
    <img src="{{ project.logo }}" class="card-img-top" alt="logo {{ project.name | downcase }}">
    {% if project.is_deprecated %}
    <img src="/images/projects/obsolete.png" class="card-img-obsolete" alt="Obsolete stamp">
    {% endif %}

  <div class="card-body">
    <h5 class="card-title">{{ project.index }}</h5>
    <h5 class="card-title">{{ project.name }}</h5>
    <p class="card-text">{{ project.description }}</p>
    <a href="{{ project.url }}" class="btn btn-primary" target="_blank" data-proofer-ignore>{{ project.cta }}</a>
  </div> 
  </div>

  {% if mod != 0 %}
    </div>
  {% endif %}
{% endfor %}