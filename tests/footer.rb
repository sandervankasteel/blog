require 'minitest/autorun'
require 'nokogiri'
require 'uri'

class TestFooterLinks < Minitest::Test
    def setup
        @doc = File.open("_site/index.html") { |f| Nokogiri::HTML(f) }
    end

    def test_that_all_footer_links_have_target_blank
        urls = @doc.css("footer .list-inline-item > a")
        for url in urls
            assert url['target'] == '_blank'
        end
    end
end