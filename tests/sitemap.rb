require 'minitest/autorun'
require 'nokogiri'
require 'uri'

class TestSitemap < Minitest::Test
    def setup
        # Open the sitemap and ignore namespaces
        @doc = File.open("_site/sitemap.xml") { |f| Nokogiri::XML(f) }        
        @doc.remove_namespaces!
    end

    def test_that_all_locs_have_an_absolute_path
        locs = @doc.xpath("//loc")
        for loc in locs
            assert loc.inner_text.index(URI.regexp)
        end
    end
end
