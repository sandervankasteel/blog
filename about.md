---
layout: page
title: About Sander van Kasteel
permalink: /about/
background: '/images/bg-about.jpg'
---

![Selfie](/images/self.jpg){:class="img-fluid float-md-right rounded"}

I'm a 30-something, self-taught developer based in Groningen, The Netherlands. My blog will primarily be about home automation (Home Assistant), (PHP, JS / Ionic and Laravel) development, Linux and some other related topics like (unit) testing, infrastructure, servers, Continuous Integration (CI) and Continuous Delivery (CD). 

If you would like to contact me, on the bottom of the pages you can find some links to some (social) media I use. But if you prefer email contact, fill out the [contact form]({{ site.url }}/contact) and I will reply to your message as soon as possible. 

PS, I like to listen to Taylor Swift, K-Pop and metal and like to wear suits.