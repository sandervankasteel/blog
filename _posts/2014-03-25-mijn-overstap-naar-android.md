---
layout: post
status: publish
published: true
title: Mijn overstap naar Android
author: Sander van Kasteel
date: '2014-03-25 03:30:30 +0100'
date_gmt: '2014-03-25 02:30:30 +0100'
categories:
- Android
tags: []
comments: []
---
<p>De afgelopen 2 jaar heb ik mij begeven in de gulden tuin van Apple inclusief AppleTV, iPad, iPhone en Macbook Pro. Wat allemaal leuk lief en aardig is, maar de tweaker in mij begint weer te kriebelen. Tijd om leuke dingen te gaan doen met hardware en software...</p>
<p>Dus ongeveer een maandje geleden, de stoute schoenen aangetrokken. Mijn iPhone verkopen en iets anders dan Apple wat naar binnen komt. Nou was mijn iPad al een tijdje de deur uit, dus nu konden mijn AppleTV en de iPhone ook gelijk de deur uit. Maar dan is de vraag, wat komt er voor in de plaats ?<!--more--></p>
<p>Windows Phone ? Nee niet mijn ding.. BlackBerry OS 10 ? Was een leuke kanshebber maar vanwege het feit dat de toestellen niet echt bij mij in de smaak vielen werd dat hem ook niet. Dus blijven er een paar opties over. Firefox OS, Symbian en Android. Firefox OS en Symbian zijn beide afgevallen omdat Symbian eigenlijk EOL (End-Of-Life) is en Firefox OS is nog in een Alpha staat. Dus is het Android geworden.</p>
<p>Maar dan is nog de vraag, wat voor een toestel. Na wat flink wikken en wegen, heel erg de stoute schoenen aangetrokken en een toestel geimporteerd uit China. De Xiaomi Mi2S &nbsp;is het geworden.</p>
<p><a href="/images/posts/826230.jpg"><img class="alignnone size-thumbnail wp-image-23" src="/images/posts/826230-150x150.jpg" alt="Xiaomi Mi2S" width="150" height="150" /></a></p>
<p>&nbsp;</p>
<p>Mijn keuze hiervoor is vooral gebaseerd op het scherm formaat en combinatie met krachtige SoC. Een 4.3 inch scherm icm een&nbsp;Snapdragon Qualcomm S600 zit er in en verslaat toch de gemiddelde mid-end tot high-end telefoon en was in mijn ogen toch al een killer combinatie. Ik wil namelijk geen 5"+ scherm en een toestel met 1 hand kunnen bedienen had toch al redelijk mijn voorkeur.</p>
<p>Na een goeie week geduld hebben stond dan de DHL meneer bij mij voor de deur met mijn pakketje (tot veel vermaak van mijn visite op dat moment ;) ). Vol plezier en verwachtingen scheurde ik de verpakking bijna compleet kapot en kon ik dan eindelijk mijn nieuwe toestel in gebruik gaan nemen.&nbsp;Het toestel voelde stevig aan, ondanks de plastic behuizing en de software voelde lekker snappy aan. En toen begon het drama van de data migratie.</p>
<p>Ongeveer een 2 weken voordat mijn Mi2S binnen zou komen heb ik al mijn data vanaf iCloud naar mijn eigen OwnCloud gemigreerd. Ik had verwacht dat alles uit iCloud krijgen een probleem zou worden, maar dat was het totaal niet. Ik kan vanuit iCalender.app en Contacts.app gewoon alles exporteren naar een iCal en CardDav formaat en netjes terug importeren naar mijn OwnCloud. Alleen alles synchroniseren vanaf mijn Android toestel naar OwnCloud installatie is een probleem.</p>
<p>Blijkbaar heeft Android standaard geen ondersteuning voor CardDav en CalDav protocollen. Dus daarvoor moest ik aparte apps voor aanschaffen en installeren. DAVDroid voor CalDav en CardDAV voor CardDav synchronisatie. Jammer daar ging punten voor Android. CardDav (contacten) ging eigenlijk vrij vlekkeloos, er waren even wat hickups omdat mijn OwnCloud installatie zelf gesigneerde SSL certificaten gebruikt maar dat was alles.</p>
<p>CalDav (agenda's) was een heel ander probleem. Door de zelf gesigneerde SSL certificaten krijg ik synchronisatie gewoon niet aan de gang. Jammer :( . Hier moet ik binnenkort nog maar eens naar kijken dan, want dit zou ik nog graag werkend willen krijgen.</p>
<p>Al met al, ben ik zeer tevreden over mijn toestel en Android :) Stiekem baal er een beetje van dat ik niet eerder overgestapt ben ;)</p>
