---
layout: post
status: publish
published: true
title: 'Hacking the Kobo Mini #3 - Remote access (FTP, Telnet en SSH)'
author: Sander van Kasteel
date: '2015-01-26 16:13:51 +0100'
categories:
- Hacking
tags:
- kobo
- kobo mini
- hacking
- linux
- busybox
- dropbear
comments: []
---
<p>Allereerst een gelukkig 2015! Laatst ben ik maar weer eens even gezellig verder gegaan met het "hacken" van mijn Kobo Mini en dit is zoals je al kan lezen aan de titel een vervolg op <a title="Hacking the Kobo Mini #2 &ndash; Hardware inspectie" href="/hacking/2014/10/09/hacking-the-kobo-mini-2-hardware-inspectie.html">deel 2</a>&nbsp;en vandaag ga ik het hebben over remote toegang tot de Kobo Mini.</p>
<p><!--more--></p>
<p>In&nbsp;mijn vorige blogpost over het hacken van de Kobo Mini heb een complete backup van mijn microSD kaart gemaakt doormiddel van het Linux / UNIX commando "dd". Maar aangezien er sinds de vorige keer en deze keer wel een paar maanden tussen zit, heb ik voor de zekerheid nog even een extra backup gemaakt van de microSD kaart want in die tijd heb ik mijn Kobo&nbsp;wel gebruikt als eReader. Dus backup maken en gaan met die banaan.</p>
<p>Ik ga als eerst wat vertellen over het software systeem wat er op de Kobo draait. Zoals al te vinden was op het internet draait er Linux op deze eReader. Om precies te zijn is dit Linux 2.6.35.3 met een BusyBox 1.22.0 als userland tools. BusyBox levert overigens wel meer als alleen wat basic userland tools.<br />
Busybox levert ook een System V init systeem, een udev achtig systeem genaamd mdev en een inetd implementatie. Al met al leuk speelgoed om mee te gaan spelen.</p>
<p>Daarnaast zijn er ook nog wat standaard UNIX tools aanwezig zoals wpa_supplicant die de connectiviteit met WiFi netwerken voor zijn rekening nemen.</p>
<p>De Kobo Mini start standaard op in een rcS modus. Dit betekent dat de Kobo altijd(!) opstart in Single-User mode en zal altijd uitvoeren wat er opgegeven word in het shell script wat staat in /etc/init.d/rcS. Dit gedrag staat netjes in /etc/inittab</p>
<blockquote><p><strong>/etc/inittab</strong><br />
# This is run first except when booting in single-user mode.<br />
::sysinit:/etc/init.d/rcS<br />
::respawn:/sbin/getty -L ttymxc0 115200 vt100<br />
::ctrlaltdel:/sbin/reboot<br />
::shutdown:/bin/umount -a -r<br />
::restart:/sbin/init</p></blockquote>
<p>En dan zal je je afvragen wat word er precies uitgevoerd bij single-user mode. Er word als eerst wat&nbsp;tests gedaan&nbsp;om welke hardware het precies gaat&nbsp;want blijkbaar draaien alle Kobo's ongeveer dezelfde software. Op&nbsp;basis daarvan word bijvoorbeeld een bepaalde firmware image ingeladen voor de WiFi module en andere hardware-specifieke zaken afgehandeld. Vervolgens worden er wat sysfs en tmpfs directories aangemaakt en gemount. Daarnaast is /etc/init.d/rcS ook verantwoordelijk voor het laten zien van het bootlogo. Maar uiteindelijk zal /etc/init.d/rcS dan de nodige&nbsp;applicatie opstarten die het echt een eReader maakt. Dit is een closed-source applicatie genaamd "nickel".</p>
<p>Nickel is niks meer dan 1 gigantisch binary blob die de eReader al zijn functionaliteit geeft. Dit begint bij het laten zien van ePub bestanden t/m de Webkit gebasseerde webbrowser en de sudoku. In tegenstelling tot wat ik vermoede in deel 1 van deze blogserie, draait er geen X.org of een X compatible server op de Kobo. MIsschien dat die toevallig verwerkt zit in de Nickel binary durf ik eigenlijk op dit moment niet zeggen.</p>
<p>Om terug te komen op het onderwerp van remote access. Gelukkig levert BusyBox zelf een aantal tools mee om remote access te regelen. BusyBox levert 2 tools mee bij hun distrubutie namelijk een telnet server (telnetd genaamd) en een FTP server (ftpd genaamd). Maar voor dat we de telnet of FTP server kunnen inschakelen moeten we als eerst maar eens zorgen dat we een inetd server hebben draaien. Dit kunnen doen door de volgende regel toe te voegen aan "/etc/inittab"</p>
<blockquote><p><b>/etc/inittab</b></p>
<p>::respawn:/usr/sbin/inetd -f /etc/inetd.conf</p></blockquote>
<p>Waardoor het eind resultaat alsvolgt er uit komt te zien.<br />
<a href="/images/posts/inittab.png"><img class="alignnone size-thumbnail wp-image-113" src="/images/posts/inittab-150x150.png" alt="inittab" width="150" height="150" /></a></p>
<p>Daarna gaan we maar eens regelen dat de FTP en de Telnet services opgestart gaan worden door inetd. Dit kunnen we door de volgende regels toe te voegen aan "/etc/inetd.conf"</p>
<blockquote><p><strong>/etc/inetd.conf</strong></p>
<p>23 stream tcp nowait root /bin/busybox telnetd -i<br />
21 stream tcp nowait root /bin/busybox ftpd -w -S</p></blockquote>
<p>Dan komt het eind resultaat voor /etc/inetd.conf uit op :</p>
<p><a href="/images/posts/inetd-pre-ssh.png"><img class="alignnone size-thumbnail wp-image-119" src="/images/posts/inetd-pre-ssh-150x150.png" alt="inetd-pre-ssh" width="150" height="150" /></a></p>
<p>Maar voor dat ftpd en telnetd volledig zullen gaan werken hebben we nog wel wat pseudoterminals nodig. Die pseudoterminals kunnen we aanmaken door een directory genaamd "pts" aan te maken in /dev, daar een devpts mount aan te mounten en dit moet aangemaakt worden tijdens het bootproces want /dev is een directory die elke keer bij boot gevuld word met alle devices en dat doen we door de volgende 2 regels toe te voegen in "/etc/init.d/rcS"</p>
<blockquote><p><strong>/dev/init.d/rcS</strong></p>
<p><span style="font-family: Georgia, 'Bitstream Charter', serif; font-style: italic;">mkdir -p /dev/pts</span></p>
<p>mount -t devpts devpts /dev/pts</p></blockquote>
<p><a href="/images/posts/rcS.png"><img class="alignnone size-thumbnail wp-image-122" src="/images/posts/rcS-150x150.png" alt="rcS" width="150" height="150" /></a></p>
<p>Ik doe dit bewust net na dat /dev gemount is zodat ik zeker weet dat telnetd en ftpd de pseudoterminals tot hun beschikking hebben zodra ze opgestart zijn. Daarna kunnen we microSD kaart terug plaatsen in de Kobo Mini, verbinding maken met&nbsp;WiFi met de Kobo Mini&nbsp;en dan kunnen we via telnet gaan inloggen. Standaard is er geen&nbsp;root wachtwoord ingesteld trouwens</p>
<p><a href="/images/posts/telnet.png"><img class="alignnone size-thumbnail wp-image-124" src="/images/posts/telnet-150x150.png" alt="telnet" width="150" height="150" /></a></p>
<p>YES! We zijn binnen. Mooi, dat is in de pocket. Laten we nu eens kijken of FTP werkt.<a href="/images/posts/ftp.png"><img class="alignnone size-thumbnail wp-image-126" src="/images/posts/ftp-150x150.png" alt="ftp" width="150" height="150" /></a></p>
<p>Vette shit! Dat werkt ook. Dat vraagt om een victorie dansje!</p>
<p><a href="/images/posts/victory.gif"><img class="alignnone size-thumbnail wp-image-130" src="/images/posts/victory-150x150.gif" alt="victory" width="150" height="150" /></a></p>
<p>Maar voor dat we verder gaan dansen, laten we eerst een paar dingen op orde hebben. Voor de veiligheid laten we de root user een andere password geven. Dit doen we door het commando "passwd".</p>
<p><a href="/images/posts/root-passwd.png"><img class="alignnone size-thumbnail wp-image-133" src="/images/posts/root-passwd-150x150.png" alt="root-passwd" width="150" height="150" /></a></p>
<p>Mooi, de root user heeft nu een password. Nu kunnen we kijken of we een SSH server aan de gang kunnen krijgen.</p>
<p>Helaasch heeft BusyBox geen implementatie van een SSH server inzich, dus zullen we die zelf maar ergens vandaan moeten halen. Nu is dit een embedded device met ook nou niet zo heel veel opslag capaciteit dus OpenSSH is eigenlijk wel te groot en te log voor de Kobo Mini.</p>
<p>De beste kandidaat voor het invullen van deze taak is Dropbear[1]. Dropbear is een heel erg kleine SSH server implementatie(~110 Kb) en werkt op een heel scala aan UNIX'en. Mooi, top, geweldig... Alleen 1 ding.. Shit.. Ik moet hem zelf compileren want er is geen binary beschikbaar voor een ARM processor. Toen was gelijk de eerste vraag "is er een C compiler ge&iuml;nstalleerd op de Kobo?". Nee, die was er ook niet. Dan zal ik maar gaan cross compilen.</p>
<p>Dan moeten we eerst maar een geschikte toolchain&nbsp;vinden. Ehhh.. Mijn eerste ingeving was de website van&nbsp;CodeSourcery en Linaro te gaan kijken. Ja ehmmm... welke versie moet ik nou precies hebben ? HardFloat, SoftFloat ?&nbsp;GCC 4.2, GCC 4.6.1, GCC 4.9.0 ?!?! Voor dat ik me in meters hoge documentatie ga&nbsp;storten, besloot ik toch maar even te gaan googlen. &nbsp;En toen vond ik het! De heilige graal! Kobo heeft alle GPL code online gezet op GitHub. [2] Great. Zit er toevallig ook nog wat tools bij om te compilen ? Ja! Great. Dan&nbsp;kan ik los!</p>
<p>Zo gezegd zo gedaan. Even een git clone gedaan van alle sources die Kobo heeft vrijgegeven. Toolchain uitgepakt en ge&iuml;nstalleerd op mijn machine en dan is het compilen geblazen. Uit ervaringen uit het verleden, wist ik dat cross compilen vrij lang kan&nbsp;duren. Dus ik zet het compile proces aan en nog voor dat ik&nbsp;opgestaan ben uit mijn bureaustoel om koffie te gaan zetten is het compilen klaar!</p>
<p>Voor het gemak heb ik mijn binaries beschikbaar gemaakt en&nbsp;je <a href="/downloads/posts/dropbear.tar.gz">hier</a>&nbsp;downloaden. Deze kan je gewoon uitpakken in / en dan heb je al 70% van al het werk gedaan. Nu nog even Dropbear configureren en dan is het klaar en dan hebben we ook nog eens SSH toegang.</p>
<p>Allereerst moeten we in /etc/ even een directory aangemaken genaamd "dropbear". Dropbear slaat daar namelijk zijn RSA en DDS (aka DSA) keys in op. Vervolgens openen we deze directory en gaan we met "dropbearkey" de 2 nodige keys genereren.</p>
<blockquote><p>cd /etc/dropbear<br />
dropbearkey -t rsa -f dropbear_rsa_host_key<br />
dropbearkey -t dss -f dropbear_dss_host_key</p></blockquote>
<p><a href="/images/posts/ssh-keygen.png"><img class="alignnone size-thumbnail wp-image-140" src="/images/posts/ssh-keygen-150x150.png" alt="ssh-keygen" width="150" height="150" /></a></p>
<p>Mooi, keys zijn gegeneerd nu. Dan is het alleen nog maar even zorgen dat inetd dropbear opstart.</p>
<blockquote><p><strong>/etc/inetd.conf</strong></p>
<p>22 stream tcp nowait root /usr/local/sbin/dropbear dropbear -i</p></blockquote>
<p><a href="/images/posts/ssh-inetd.png"><img class="alignnone size-thumbnail wp-image-143" src="/images/posts/ssh-inetd-150x150.png" alt="ssh-inetd" width="150" height="150" /></a></p>
<p>&nbsp;</p>
<p>Check! Nu kunnen we de machine rebooten (of inetd opnieuw starten) en dan zou dropbear netjes opstarten en kunnen we via SSH remote inloggen op de Kobo.</p>
<p><a href="/images/posts/ssh-root.png"><img class="alignnone size-thumbnail wp-image-144" src="/images/posts/ssh-root-150x150.png" alt="ssh-root" width="150" height="150" /></a></p>
<p>Echter moeten we nog 1 kleine fix toepassen en dan zijn we klaar! Als we namelijk over telnet inloggen met de root user krijgen we een andere $PATH dan dat als we via SSH inloggen. Dropbear haalt zijn $PATH uit /etc/profile dus die zullen we even moeten aanpassen om te zorgen dat zowel telnet en&nbsp;SSH dezelfde $PATH meekrijgen.</p>
<blockquote><p><strong>/etc/profile</strong></p>
<p>PATH=$PATH:/sbin:/bin:/usr/sbin:/usr/bin</p></blockquote>
<p><a href="/images/posts/profile_path.png"><img class="alignnone size-thumbnail wp-image-146" src="/images/posts/profile_path-150x150.png" alt="profile_path" width="150" height="150" /></a></p>
<p>En klaar is Klara! We hebben nu SSH, FTP en Telnet toegang tot de Kobo.&nbsp;Ik durf echter niet met zekerheid te zeggen of het ook werkt op andere Kobo eReaders. Een bekende van mij heeft de Kobo Aura en na wat Googlen kwamen we er achter dat er in de Kobo Mini en de Kobo Aura dezelfde processor zit alleen de processor in de Kobo Mini is geklokt op 800 Mhz en die in de Kobo Aura is geklokt op 1 Ghz.</p>
<p>Dan nu maar eens Python gaan compileren!</p>
<p>1.&nbsp;<a title="https://matt.ucc.asn.au/dropbear/dropbear.html" href="https://matt.ucc.asn.au/dropbear/dropbear.html" target="_blank">https://matt.ucc.asn.au/dropbear/dropbear.html<br />
</a>2.&nbsp;<a title="https://github.com/kobolabs/Kobo-Reader" href="https://github.com/kobolabs/Kobo-Reader" target="_blank">https://github.com/kobolabs/Kobo-Reader</a></p>
