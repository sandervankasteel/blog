---
layout: post
status: publish
published: true
title: 'Home automation - #1 De ontdekkingsreis'
author: Sander van Kasteel
date: '2015-04-14 17:51:05 +0200'
categories:
- Electronics
- Arduino
tags: []
comments: []
---
<p>Al een flinke tijd&nbsp;ben ik aan na het nadenken over domotica (ook wel home automation genoemd, <a href="https://en.wikipedia.org/wiki/Home_automation" target="_blank">wikipedia</a>) en hoe ik het precies wil gaan indelen en gaan aanpakken.</p>
<p>Nu woon ik zelf gelukkig niet zo heel erg groot (ongeveer 26m2 exclusief badkamer en keuken) dus valt het allemaal wel weer mee hoeveel sensoren ik precies nodig ga hebben.</p>
<p>Maar ik loop vooruit op de zaken. Zoals ik ook al zei, woon ik niet zo heel erg groot. Ik woon in een studio appartement met een aparte badkamer en een "aparte" keuken. Hier onder vind&nbsp;zie je de indeling van mijn 1 kamer appartement.</p>
<p><a href="/images/posts/indeling.jpg"><img class="alignnone size-full wp-image-204" src="/images/posts/indeling.jpg" alt="indeling" width="385" height="328" /></a></p>
<p>&nbsp;</p>
<p><!--more-->Maar ik heb inmiddels mijn eerste concept eisenpakket al opgesteld:<br />
<strong>Woonkamer/slaapkamer</strong></p>
<ul>
<li>Licht aan en uit schakelen op basis van de hoeveelheid licht in de kamer</li>
<li>Temperatuur beheersing</li>
<li>Gordijnen open en dicht kunnen doen</li>
</ul>
<p><strong>Badkamer en gang</strong></p>
<ul>
<li>Licht aan en uit schakelen op basis van beweging</li>
</ul>
<p><strong>Keuken</strong></p>
<ul>
<li>Licht aan en uit schakelen op basis van beweging</li>
</ul>
<p>Dit is op dit moment het basis lijstje maar daarnaast wil ik&nbsp;eigenlijk ook nog alles&nbsp;kunnen aansturen via&nbsp;een webinterface.&nbsp;Dus alles moet ook nog eens een connectie hebben met mijn WiFi netwerk.</p>
<p>Maar voor dat ik zoiets echt kan gaan bouwen, zal ik eerst maar eens even een prototype gaan moeten maken. Als eerste proof of concept wil ik een LED aan en uit schakelen op basis van de hoeveelheid licht in de kamer.</p>
<p>Voor deze proof of concept heb ik een aantal standaard componenten gebruikt.</p>
<ul>
<li>1x Licht gevoeligeweerstand (ook wel een LDR genoemd,&nbsp;<a href="https://en.wikipedia.org/wiki/Photoresistor" target="_blank">wikipedia</a>)</li>
<li>1x Rode LED</li>
<li>1x 220 Ohm weerstand</li>
<li>1x 10k Ohm weerstand</li>
</ul>
<p>Hier onder vind je de&nbsp;aansluitschema's hoe alles aangesloten zit op&nbsp;de Arduino.</p>
<p><a href="/images/posts/breadboard.png"><img class="alignnone size-large wp-image-200" src="/images/posts/breadboard-1024x381.png" alt="breadboard" width="584" height="217" /></a></p>
<p><a href="/images/posts/schematic-e1429005048576.png"><img class="alignnone wp-image-202 size-large" src="/images/posts/schematic-1024x609.png" alt="schematic" width="584" height="347" /></a></p>
<p>Dit schema werkt op basis van een spanningsdeler (Wikipedia <a href="https://en.wikipedia.org/wiki/Voltage_divider" target="_blank">Engels</a> / <a href="https://nl.wikipedia.org/wiki/Spanningsdeler" target="_blank">Nederlands</a>), dus zodra er meer licht op de LDR komt zal er minder weerstand optreden waardoor een lagere waarde uit te lezen valt&nbsp;op&nbsp;AnalogPin 0. De Arduino zal vervolgens de rest van automatisering van zijn rekening nemen. Dit&nbsp;wordt gedaan met de onderstaande code;</p>
<p>[code language="cpp"]<br />
int LDR_Pin = A0; //analog pin 0<br />
int led = 12;</p>
<p>void setup()<br />
{<br />
    Serial.begin(9600);<br />
    pinMode(led, OUTPUT);<br />
}</p>
<p>void loop()<br />
{<br />
    int LDRReading = analogRead(LDR_Pin);</p>
<p>    Serial.println(LDRReading);<br />
    delay(250); //just here to slow down the output for easier reading</p>
<p>    if (LDRReading >= 600)<br />
    {<br />
        digitalWrite(led, HIGH);<br />
    } else {<br />
        digitalWrite(led, LOW);<br />
    }<br />
}<br />
[/code]</p>
<p>Zodra de uitleeswaarde van AnalogPin 0 meer of gelijk is aan 600 (dan is het donker) dan zal de Arduino de&nbsp;LED aanschakelen (door hem te voorzien van 5 volt). Daarnaast heb ik voor het uitvinden van de waarde waarop ik wil schakelen een Serial.println() toegevoegd&nbsp;zodat ik in de Arduino IDE via de seri&euml;le monitor de waarde op dat moment kan bekijken. Zo ziet het er dan als volgt uit.</p>
<p><a href="/images/posts/LDR-3.png"><img class="alignnone size-full wp-image-201" src="/images/posts/LDR-3.png" alt="LDR-3" width="876" height="370" /></a></p>
<p>En dan is het nu tijd voor het&nbsp;moment supr&ecirc;me. Werkt het ?</p>
<iframe src="https://www.youtube.com/embed/72bJpS_57_Y" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<p>Ja het werkt! Nu is dit wel een proof of concept en is dit alles behalve realistisch en betrouwbaar. Een LDR is goedkoop (20 stuks voor $1,- via eBay) maar de resultaten die je hier uit krijgt is niet heel erg betrouwbaar want als er al een schaduw op de LDR valt dan zal deze al meer weerstand geven terwijl er eigenlijk vrij weinig echt veranderd is aan de hoeveelheid aan licht in de kamer.<br />
Mocht je het betrouwbaarder willen hebben (dus het aantal lumen willen meten), dan zijn er een aantal andere mogelijkheden. Je zou een TSL230R, TSL2561 of een TEMT6000 kunnen gebruiken hiervoor en die zijn al een stuk accurater.</p>
<p>Maar het is op dit moment nog maar&nbsp;een proof of concept. Mijn eerst volgende stap is het toevoegen van WiFi aan deze proof of concept zodat ik via een webinterface de LED kan gaan schakelen. De WiFi ga ik toevoegen door middel van een ESP8266 module. Maar daar ga ik het binnenkort eens over hebben zodra ik hem binnen heb en dan gaan wij hier mee verder!</p>
