---
layout: post
status: publish
published: true
title: Hacking the Kobo Mini
author: Sander van Kasteel
date: '2014-09-25 20:37:07 +0200'
categories:
- Hacking
tags: []
---
<p>Ik ben zoals altijd opzoek naar nieuwe projecten. Dit maal is mijn&nbsp;slachtoffer een Kobo Mini geworden.</p>
<p><a href="/images/posts/kobo-hive5.jpg" rel="lightbox[kobo]"><img class="alignnone size-medium wp-image-43" src="/images/posts/kobo-hive5-300x200.jpg" alt="kobo-hive5" width="300" height="200" /></a></p>
<p><strong>INTRODUCTIE</strong></p>
<p>De Kobo Mini is een vrij simpele en goedkope e-ink eReader. De Kobo Mini&nbsp;draait op een Freescale <span style="color: #000000;">ARM Cortex A8, een</span>&nbsp;vrij simpele ARM8 (ARMv7&nbsp;architecteur) [1] ondersteund met 256 Mb RAM en 2 Gb opslag, die volgens internet bestaat de data opslag uit 1x maal een MicroSD&nbsp;kaart [2]. Daarnaast heeft die een 5" touchscreen met een resolutie van 600x800 pixels. De aanschijfprijs van&nbsp;een Kobo Mini ligt rond de 40 euro. Vrij goedkoop voor een eReader.<!--more--></p>
<p>De software die op dit moment draait op de Kobo Mini is vrij&nbsp;minimalistisch. Je kan er eBooks oplezen en daarnaast heb je nog een aantal&nbsp;"Beta functies" zoals een potje schaak of sudoku is mogelijk. Maar daarnaast heb je&nbsp;ook nog een schetsblok en heb je een WebKit gebasseerde browser tot je&nbsp;beschikking. Onder deze gui draait er een custom Linux kernel met een basic userland toolkit [1], naar wat ik vermoed is dit BusyBox. Daarnaast&nbsp;verwacht ik een X.org (of een andere X implementatie) te vinden waar bovenop<br />
de launcher / GUI draait.</p>
<p><a href="/images/posts/IMG_20140925_212017.jpg" rel="lightbox[kobo]"><img class="alignnone size-medium wp-image-44" src="/images/posts/IMG_20140925_212017-222x300.jpg" alt="IMG_20140925_212017" width="222" height="300" /></a></p>
<p>Allemaal vrij basic en niks spetterends dus. Het lijkt mij veel leuker om functies&nbsp;toe te kunnen voegen zoals een Twitter client, een zelfgeschreven launcher, misschien&nbsp;wel leuke dingen gaan doen met de USB poort of Wi-Fi synchronisatie kunnen doen met Calibre.&nbsp;Maar voor dat er uberhaupt aan dit begonnen kan worden, moeten er eerst een aantal&nbsp;dingen duidelijk zijn en geregeld worden. En juist dit hele process wil ik gaan bij&nbsp;houden in blog vorm.</p>
<p>Het doel van dit hele project is mijn eigen code kunnen runnen op een Kobo Mini en inzicht te krijgen in de software architecteur van de Kobo. Uiteindelijk moet deze code ook vrij beschikbaar zijn via een GNU/GPL licentie.</p>
<p>Wat ik in dit geval beschouw als eigen code is niks meer dan een Hello World<br />
in een Python script. Dus ik moet op zijn minst een Python interpreter kunnen<br />
draaien op de Mini om dit project enigzins successvol te laten aflopen.</p>
<p>Dus als eerste, hardware open schroeven en kijken wat er aan de binnenkant zit!</p>
<p>Bronnen:<br />
[1]&nbsp;<a href="https://en.wikipedia.org/wiki/Kobo_Mini" target="_blank">http://en.wikipedia.org/wiki/Kobo_Mini</a><br />
[2]&nbsp;<a href="http://www.mobileread.com/forums/showthread.php?t=193321" target="_blank" data-proofer-ignore>http://www.mobileread.com/forums/showthread.php?t=193321</a></p>
