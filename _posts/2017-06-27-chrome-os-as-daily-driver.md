---
layout: post
status: publish
published: true
title: Chrome OS as daily driver
author: Sander van Kasteel
date: '2017-06-27 12:00:47 +0200'
categories:
- Geen categorie
tags: []
comments: []
---
<p>As a student, techhead and (part-time) web developer, I&rsquo;m always on the lookout for new things. New things
  that will improve my life or my productivity. For the last 4 years, I&rsquo;ve been using my trusty Macbook Pro
  13&rdquo; from 2012 as my daily driver. At work, at school and for a while as my desktop machine. I really enjoy
  using that machine, but it my MBP is growing old. With the upgrade I did 2 years back (a Samsung 850 SSD and upgraded
  to 16 gigs of RAM), I managed to extend the lifetime of my MBP with a few more years. But&rsquo;s only a matter of
  time before my trusty old MBP isn&rsquo;t capable anymore to do the tasks I require of it.</p>
<p>So, it&rsquo;s time to look at a new machine. I&rsquo;ve nearly made up my mind, my new on the go driver isn&rsquo;t
  gonna be a OS X / MacOS based machine. Mainly due to the fact, that I can&rsquo;t upgrade anything more on the
  machine after I bought it. Time to get my feet wet, and try something that isn&rsquo;t an usual pick for a web
  developer.</p>
<p>I was talking to a friend of mine, and he graciously allowed me to borrow his &ldquo;old&rdquo; Chromebook. An Acer
  Chromebook 14, CB3-431 to be exact and I&rsquo;m gonna challenge myself. I&rsquo;m gonna use this machine for the
  next 7 days, starting today and use it as my daily driver for all my school and spare-time development work. I say
  spare-time development with a reason, for my work I simply cannot replace my Macbook just yet. But my experiences
  with this Chromebook will be taken into consideration for a new machine.</p>
<p>So the first task to do is get the Chromebook into developers mode and install crouton (
  github.com/dnschneid/crouton ). Crouton is an application which allows you to install different chroots and install
  applications within those chroots. Some examples of these applications are Gimp, VS Code or even a full blown desktop
  environment like Gnome or XFCE. Crouton even has some prepackaged chroot&rsquo;s ready which includes XFCE4, but I
  digress.</p>
<p>By using crouton I&rsquo;m able to setup a chroot with a base Linux distribution ( most likely Debian based), which
  will suit my own personal needs. My current needs are an editor, PHP, MySQL / MariaDB and NodeJS and yarn. Normally
  as editor I would have picked my trusty PHPStorm, but I&rsquo;m starting to think that PHPStorm is a bit to heavy for
  the CPU that&rsquo;s in this Chromebook CPU (an Intel Celeron N3160), so I&rsquo;ve chosen to use my second pick
  editor, Visual Studio Code.</p>
<p>I&rsquo;m imagining that will bump into some issues with crouton, but that beside that I will have little to no
  serious issues. Maybe some inconveniences, but that will be about it. Nothing all too serious, mainly because my 90%
  of the things I do are already on the web.</p>
<p>Alright, I think it&rsquo;s time for me to switch. Bye bye MacOS, I will see in a week!</p>