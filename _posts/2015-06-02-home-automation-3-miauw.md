---
layout: post
status: publish
published: true
title: 'Home Automation #3 - MIAUW!'
author: Sander van Kasteel
date: '2015-06-02 15:45:46 +0200'
categories:
- Android
- Programmeren
- Hardware
- Electronics
- Arduino
- Python
- Home automation
tags: []
comments: []
---
<p>In de afgelopen paar weken ben ik flink bezig geweest met programmeren&nbsp;van de webinterface en de daarbij behorende backend. Maar het belangrijkste van alles! Het project heeft een codenaam gekregen, namelijk Project Skittles. De reden dat het Project Skittles heet is eigenlijk heel simpel, ik wist&nbsp;niks beters en mijn kat heet ook zo ;)</p>
<p><a href="/images/posts/skittles.jpg"><img class="alignnone size-large wp-image-310" src="/images/posts/skittles-1024x633.jpg" alt="skittles" width="584" height="361" /></a></p>
<p><!--more--></p>
<p>Allereerst ben ik begonnen met maken van de webinterface en het toevoegen van een webserver aan het gehele pakket. Mijn eerste idee om mijn gehele automatiseringssysteem in PHP te schrijven, is in het water gevallen omdat de ESP8266 geen HTTP request kan hebben door de beperkte buffergrootte waardoor er corruptie van de data ontstaat en daarnaast vond ik dit een goed excuus om mijn roestige Python kennis bij te spijkeren. Dus heb ik er toen voor gekozen om zelf een simpel TCP protocol te gaan bedenken en implementeren. Sinds de vorige "versie" van het gehele systeem heb ik nog niks echt veranderd aan de implementatie van het protocol.</p>
<p>Om het systeem beheerbaar te maken via HTTP, heb ik besloten&nbsp;om <a href="http://www.cherrypy.org/" target="_blank">CherryPy</a> mee te leveren. CherryPy is een Python module die het mogelijk maakt om een HTTP server op te bouwen en te configureren naar jouw eisen / wensen.&nbsp;Als template engine heb ik gekozen voor <a href="http://jinja.pocoo.org/docs/dev/" target="_blank">Jinja 2</a>,&nbsp;voor de HTML layout heb ik gekozen voor <a href="http://getbootstrap.com" target="_blank">Bootstrap</a> en grafische elementen heb ik gekozen voor <a href="https://fontawesome.com/" target="_blank">Font Awesome</a>. De reden dat ik gekozen heb voor Bootstrap is omdat Bootstrap mij super makkelijk een responsive&nbsp;layout geeft en ik me dus geen zorgen meer hoef te maken over CSS, verschillende resoluties etc etc.</p>
<p>In de vorige "versie" van mijn server, werden alle statussen weggeschreven in files en eigenlijk vond ik eigenlijk toch niet zo heel erg handig dus heb ik besloten om alles weg te schrijven in een MySQL database.</p>
<p>En dit is dan&nbsp;het eind resultaat van mijn Bootstrap "webdesign". Webdesign staat met een reden in quotes want ik ben verre van een webdesigner ;)<a href="/images/posts/lights.png"><img class="alignnone size-large wp-image-311" src="/images/posts/lights-1024x640.png" alt="lights" width="584" height="365" /></a></p>
<p><a href="/images/posts/Screenshot-from-2015-06-02-10-21-04.png"><img class="alignnone size-large wp-image-320" src="/images/posts/Screenshot-from-2015-06-02-10-21-04-1024x640.png" alt="Screenshot from 2015-06-02 10-21-04" width="584" height="365" /></a></p>
<p>Dit zijn&nbsp;op dit moment de enige 2 pagina's die beschikbaar zijn en enigzins werken. De werking zal verder worden uitgewerkt zodra ik het systeem verder uitgedacht heb. Op de 'lampen' pagina, is het mogelijk om lampen aan en uit te zetten, maar nog niet verwijderen en/of toevoegen. Het toevoegen en/of verwijderen van lampen moet nog gebeuren de MySQL database. Het aan/uitzetten van lampen gebeurt&nbsp;via AJAX.</p>
<p>De data die op dit moment zichtbaar is op de 'temperatuur' pagina, is op dit moment allemaal test data en doet in mijn systeem nog helemaal niks want ik heb daarvoor nog niet de juiste onderdelen.</p>
<p>Ik ben mij vooral aan het focussen geweest op de API en een bijbehorende Android app. De API is op dit moment poepje simpel.</p>
<p><strong>GET /api/lights</strong><br />
Deze functie&nbsp;retourneert een array met Javascript objecten met daarin de naam en de huidige status. De huidige status&nbsp;is een boolean, dus deze kan alleen maar aan of uit zijn (true of false).</p>
{% highlight json %}
{% raw %}
[
      {
            'name': 'kitchen',
            'status': 'false'
      },
      {
            'name': 'gang',
            'status': 'false'
      },
      {
            'name': 'kamer',
            'status': 'false'
      }
]
{% endraw %}
{% endhighlight %}
<p><strong>POST /api/light/update</strong><br />
Met deze functie kan je een lamp een nieuwe status geven. Deze functie heeft op dit moment geen return, ook niet bij een fout.<br />
Bijvoorbeeld;</p>
<p><strong>POST</strong></p>
{% highlight json %}
{% raw %}
{
    'lightname' : 'kitchen',
    'status' : 'true'
}
{% endraw %}
{% endhighlight %}
<p><strong>POST /api/temperature</strong><br />
Met deze functie kan je de huidige temperatuur opvragen binnen een kamer en zal bij succes een JSON object retourneren. De temperaturen die geretourneerd worden zijn in Celsius.<br />
<strong>POST</strong></p>
{% highlight json %}
{% raw %}
{
    'room' : 'gang'
}
{% endraw %}
{% endhighlight %}
<p><strong>RETURN</strong></p>
{% highlight json %}
{% raw %}
{
    'room': 'gang',
    'temperature': 20.5
}
{% endraw %}
{% endhighlight %}
<p><strong>GET /api</strong><br />
Met deze functie kan je kijken of de API en daarmee ook de server nog online is. Deze functies retourneert een JSON object indien succesvol.</p>
{% highlight json %}
{% raw %}
{
      'miauw': 'said skittles'
}
{% endraw %}
{% endhighlight %}
<p>En dat zijn op dit moment de API functies. In de loop van de tijd wil de API nog verder gaan uitbreiden. Maar voor het moment voldoet dit wel. Met deze beperkte API set,&nbsp;ben ik al wel in staat geweest om een Android app te ontwikkelen. Hieronder zie je een screenshot tijdens de ontwikkeling van de huidige versie.&nbsp;<a href="/images/posts/Screenshot-from-2015-05-26-16-22-01.png"><img class="alignnone size-large wp-image-330" src="/images/posts/Screenshot-from-2015-05-26-16-22-01-1024x684.png" alt="Screenshot from 2015-05-26 16-22-01" width="584" height="390" /></a></p>
<p>Inmiddels is Android app qua ontwikkeling al wat verder gevorderd en is het inmiddels mogelijk om de status van de lampen te bekijken en te veranderen. Uiteraard hoort daar ook een demotje bij, zie hiervoor het onderstaande YouTube filmpje.</p>
<iframe src="https://www.youtube.com/embed/AEFaTG3k7zQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<p>Nu het project verder en verder begint te komen, wordt het tijd om weer eens wat inkopen te gaan doen. Allereerst heb ik een voeding nodig om de hele boel te voorzien van stroom. Mijn eerste gedachte was om zelf een voeding te gaan ontwerpen op basis van de HLK-PM01. Voor de mensen die de HLK-PM01 niet kennen, het is een gereguleerde power module die het mogelijk maakt om 1 keer van 230 Volt AC naar 5 Volt DC te gaan. Dus dan hoef je niet zelf te gaan knutselen om een bruggelijkrichter (bridge rectifier in het Engels) te gaan bouwen en alle bijbehorende beveiliging.&nbsp;Het enige nadeel is de prijs. De module alleen al is op eBay te vinden voor ~ 6 dollar en dan moet er&nbsp;nog overspanning en kortsluiting beveiliging bij. Nog een beperking van de HLK-PM01 de output is beperkt tot 5V @ 600 mA. Na een kort rekensommetje is 600 mA toch wat aan de krappe kant, aangezien ik op ongeveer 550 mA uitkom met al mijn onderdelen (inclusief veiligheidsmarge). Dus heb ik besloten om een kant en klare voeding in China te kopen.</p>
<p>Na wat zoekwerk kwam ik uit op deze <a href="https://www.ebay.com/itm/171736846864" target="_blank">voeding</a>, typenummer XK-1205DC. De verkoper beweert dat deze gereguleerde voeding maximaal 4 A kan leveren (nominaal tussen de 2 en 4 A) en deze voeding is ook nog eens een stuk goedkoper dan den HLK-PM01. De XK-1205DC kost namelijk slechts $&nbsp;3,83 (ten tijde van schrijven). Dus ik ben benieuwd wat ik voor dat bedrag ga krijgen ;). Zolang die al 1000mA of meer kan leveren ben ik al tevreden.</p>
<p>Als 230V relay heb ik gekozen voor de&nbsp;Songle SRD-05VDC-SL. Dit is een relais bedoelt voor het gebruik in Arduino en andere micro processoren omgeving. Hij heeft 1 digitale input waarmee je de relais kan laten schakelen tussen aan en uit.</p>
<p>Daarnaast heb ik nog een aantal andere dingen besteld vanuit China een DHT22 (temperatuur en luchtvochtigheid meter), een HC-SR501&nbsp;PIR sensor (een sensor bedoelt voor het detecteren van beweging) en een setje van 2x 28BYJ-48 stepper motor inclusief ULN2003 driver board. Mijn plan is om deze stepper motors te gaan gebruiken voor het openen en dichtdoen van mijn gordijnen. Ik weet niet of ze daar krachtig genoeg voor zijn en hoe ik dit mechanisch ga aanpakken.</p>
<p>Maar het leukste komt nog. Al een flinke tijd droom ik van het hebben van een oscilloscoop, alleen iedere keer als ik op Marktplaats kijk staan ze er voor of belachelijk veel geld op. 200 euro voor een 2de hands analoge Tektronix (exclusief probes) en dan kan ik net zo goed een Owon / Rigol&nbsp;voor ~ 300 euro kopen of als ik ze goedkoper vind, dan zijn het meestal compleet afgetrapte Hameg / Philips&nbsp;oscilloscopen en die kosten dan nog tussen de 75&nbsp;en 125 euro waarvan de werking nog maar te betwijfelen valt. En dat vind ik voor een stuk gereedschap waarvan ik betwijfel of ik hem ook echt vaak zal gebruiken redelijk aan de prijzige kant.</p>
<p>Nu was ik gisteren middag even aan het rondspeuren op <a href="https://www.banggood.com/?p=TZ081713301692015034" target="_blank">Banggood.com</a> en vond ik een kit van een <a href="http://www.banggood.com/DIY-Digital-Oscilloscope-Kit-Electronic-Learning-Kit-p-969762.html?p=TZ081713301692015034" target="_blank">oscilloscoop</a>&nbsp;voor &euro;21.63 . Om precies te zijn is dit de <a href="http://www.jyetech.com/Products/LcdScope/e138.php" target="_blank" data-proofer-ignore>JYE Tech DSO 138</a>. Stiekem heeft dit wel mijn interesse gewekt. Het is een oscilloscoop kit met een maximale analoge bandbreedte van 200 Khz, maximaal 1 miljioen samples per seconde, een maximale peak-to-peak input van 50V, triggering en DC,AC en GND&nbsp;coupling. Nu weet ik zelf dat het geen vervanging is voor een echt oscilloscoop maar voor het geld, vind ik het best grappig. Dus die heb ik ook nog besteld en dan kan ik die gelijk ook even op de pijnbank / reviewbank leggen ;)</p>
<p>Maar zodra al deze onderdelen binnen zijn, kan ik mijn huidige prototype eens verder gaan uitbouwen en hopelijk snel gaan verwerken tot echt werkende module inclusief professioneel&nbsp;gefabriceerde PCB en behuizing.</p>
<p>Tot die tijd staat er echter nog genoeg op mijn ToDo lijst. Zo wil ik alle componenten binnen Project Skittles kunnen toekennen aan een ruimte. Bijvoorbeeld "lamp x" hoort bij ruimte "gang". Daarnaast staan er nog een paar dingen op het lijstje.&nbsp;De mogelijkheid tot bijvoorbeeld het updaten van de temperatuur vanaf het TCP protocol. Ik wil ook nog leuke grafiekjes kunnen genereren zoals gemiddelde brandtijd van lamp x per dag, gemiddelde temperatuur per dag etc etc. Ondersteuning&nbsp;voor LED strips. Al met al genoeg te doen dus!</p>
<p>Mocht&nbsp;je ge&iuml;nteresseerd zijn in de source en eventueel kritiek hebben op mijn Python, bijna alles staat op <a href="https://github.com/sandervankasteel/skittles-application">Github</a>.&nbsp;Het enige wat op dit moment nog niet op Github staat, is de Arduino code voor de "appliances" (de Arduino module). Maar die zal ik in de loop van de dag online zetten.</p>
<p>Ik hoop dat dit lange verhaal een beetje leuk was om te lezen en tot de volgende keer! :)</p>
