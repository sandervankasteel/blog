---
layout: post
status: publish
published: true
page_title: Recept voor frambozen taart of hoe ik een Raspberry Pi gerepareerd heb
title: Hoe ik een Raspberry Pi gerepareerd heb
author: Sander van Kasteel
date: '2015-05-17 18:12:53 +0200'
categories:
- Electronics
- Raspberry Pi
tags: []
comments:
- id: 3
  author: Vincent de Koning
  author_email: mail@vincentdekoning.nl
  author_url: ''
  date: '2015-05-17 22:07:32 +0200'
  date_gmt: '2015-05-17 20:07:32 +0200'
  content: Sander, leuke post om te lezen. Bedankt voor je hulp en vakwerk! Groeten,
    Vincent
---
<p>Ongeveer een 2 weken geleden, zag ik op de Vraag &amp; Aanbod sectie van Tweakers een advertentie voorbij komen voor een beschadigde Raspberry Pi B. Het SD kaart slot begon kuren te vertonen en hij heeft geprobeerd deze zelf te vervangen.</p>
<p>Oorspronkelijk bood de betreffende persoon hem te koop aan. Toch&nbsp;heb ik&nbsp;de betreffende persoon even een DM gestuurd met de vraag;</p>
<blockquote><p>Ik zag net je advertentie voor je Raspberry Pi en vroeg me eigenlijk af wil je echt verkopen of heb je liever dat iemand hem repareert ?De reden waarom ik dit vraag is omdat ik dit zelf wel als uitdaging zie om te repareren maar ik ben zelf niet op zoek naar een nieuwe Raspberry Pi.</p></blockquote>
<p>De des betreffende persoon reageerde vol opluchting dat iemand zijn Pi wou repareren. Na&nbsp;wat DM's heen en weer over details en een vergoeding (in dit geval een zak M&amp;M's) lag zijn Pi 2 dagen later bij mij in de brievenbus met een extra SD kaart slot er bij.</p>
<p><!--more--></p>
<p><a href="/images/posts/DSC_7851.jpg"><img class="alignnone size-large wp-image-279" src="/images/posts/DSC_7851-1024x685.jpg" alt="DSC_7851" width="584" height="391" /></a> <a href="/images/posts/DSC_7853.jpg"><img class="alignnone size-large wp-image-280" src="/images/posts/DSC_7853-1024x685.jpg" alt="DSC_7853" width="584" height="391" /></a></p>
<p>Er was sowieso al 1 soldeereiland helemaal verbrand, dus die verbinding moest eigenlijk al verlegd worden met een stukje kabel.</p>
<p>Nouja, laat ik als eerst maar eens even het oude SD kaart slot er af solderen. Dat ging opzich wel prima met mijn Weller soldeerbout, alleen heb ik zelf ook nog 1 soldeereiland ook verbrand. Of hij was ook al verbrand was, dat durf niet te zeggen.&nbsp;<a href="/images/posts/DSC_7856.jpg"><img class="alignnone size-large wp-image-283" src="/images/posts/DSC_7856-1024x685.jpg" alt="DSC_7856" width="584" height="391" /></a> <a href="/images/posts/DSC_7861.jpg"><img class="alignnone size-large wp-image-284" src="/images/posts/DSC_7861-1024x685.jpg" alt="DSC_7861" width="584" height="391" /></a></p>
<p>Dan ga ik maar eerst even beginnen met uitzoeken welke 2 eilandjes missen.&nbsp;Gelukkig heeft Raspberry Foundation alle <a href="https://www.raspberrypi.org/app/uploads/2012/10/Raspberry-Pi-R2.0-Schematics-Issue2.2_027.pdf" target="_blank">schema's</a> vrij gegeven en heb ik zelf een Raspberry Pi B, dus kon ik zo met de een multimeter uitvinden welke 2 lijnen er missen. Dat waren de lijnen SD_CMD en SD_DATA1 (gaan richting R48 en R50 resp.)</p>
<p>Toen daarna maar begonnen met het solderen van het SD kaart slot op de eilandjes die in ieder geval nog wel goed waren (en niet verbrand). Dat ging allemaal voorspoedig. Alleen kwam ik met 2 "uitdagingen" te zitten. De pinout op het SD kaart slot kwam niet helemaal overheen met het gebruikte SD kaart slot van de Raspberry Pi. De "card detect" pinnen en de "write lock" pinnen komen op dit slot niet overheen met de PCB van de Raspberry Pi. Gelukkig weet het schema ons te vertellen dat de de "write lock" pinnen niet gebruikt worden en om het probleem van de "card detect" pinnen heen te werken heb ik besloten om ze altijd contact met elkaar te laten hebben. Daardoor denkt de Pi dus altijd dat er een SD kaart in zit.</p>
<p>Nu&nbsp;eerst maar even de uitdaging van de 2 verbrande eilandjes aanpakken. De verbinding tussen pin 5 en R48 was zo gelegd. Het was even een piel werkje maar dan heb je ook wat! :D Een net en kort draadje tussen de weerstand en de pin van het SD kaart slot.</p>
<p><a href="/images/posts/DSC_7878.jpg"><img class="alignnone size-large wp-image-285" src="/images/posts/DSC_7878-1024x685.jpg" alt="DSC_7878" width="584" height="391" /></a></p>
<p>Toen kwam de grootste uitdaging. Die ene data lijn (SD_DATA1). Mijn eerste gedachte was om hem net zoals SD_CMD direct op de weerstand te solderen. Maar ik kreeg op geen enkele manier mijn draadje gesoldeerd op die weerstand (R50). Mijn vermoeden is dan dus ook dat ook dat soldeereiland verbrand is, maar dat heb ik echter niet kunnen controleren want ik heb geen microscoop.<br />
Dan zit er geen andere optie op, dan die weerstand te bypassen en hem direct op de "uitgang" van de weerstand.&nbsp;Een uitgang is er natuurlijk niet bij een passief component zoals een weerstand, vandaar dat die ook in quotetekens staat ;)</p>
<p>Vervolgens heb ik als trekbeveiliging over het SD kaart slot heen even een kabeltje heen getrokken. &nbsp;En dan is dit&nbsp;het eind resultaat!</p>
<p><a href="/images/posts/DSC_7879.jpg"><img class="alignnone size-large wp-image-286" src="/images/posts/DSC_7879-1024x685.jpg" alt="DSC_7879" width="584" height="391" /></a></p>
<p><a href="/images/posts/IMG_20150506_143559.jpg"><img class="alignnone size-large wp-image-303" src="/images/posts/IMG_20150506_143559-768x1024.jpg" alt="IMG_20150506_143559" width="584" height="779" /></a></p>
<p>Maar nu is de vraag... Doet die het ook ?</p>
<p><a href="/images/posts/IMG_20150506_143020.jpg"><img class="alignnone size-large wp-image-298" src="/images/posts/IMG_20150506_143020-768x1024.jpg" alt="IMG_20150506_143020" width="584" height="779" /></a></p>
<p>Volgens mij heb ik nu wel mijn zak M&amp;M's verdiend ;)</p>
<p><img class="alignnone size-large wp-image-302" src="/images/posts/IMG_20150506_214848-e1431879072482-1024x768.jpg" alt="IMG_20150506_214848" width="584" height="438" /></p>
