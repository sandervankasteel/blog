---
layout: post
status: publish
published: true
title: JYE Tech DSO 138 - Review / buildlog
author: Sander van Kasteel
date: '2015-06-19 00:01:03 +0200'
categories:
- Hardware
- Electronics
- Reviews
tags:
- DSO 138
- Electronics
comments: []
---
<p>Voor de mensen die mij op <a href="https://twitter.com/Hertog6" target="_blank" data-proofer-ignore>Twitter</a> volgen, wisten het al een paar dagen. Maar ik heb dan eindelijk mijn oscilloscoop kit binnen gekregen van <a href="http://www.banggood.com/?p=TZ081713301692015034" target="_blank">Banggood</a>.</p>
<p>Maar de vraag is nu een beetje, wat krijg je nou voor ongeveer 20 euro ?</p>
<p><!--more--></p>
<p>Voor ongeveer 20 euro krijg je een kit om zelf je oscilloscoop in elkaar te solderen. Dit geheel wordt geleverd in 1 grote grip lock zak met daarin wat kleinere grip lock zakjes.</p>
<p><a href="/images/posts/IMG_0174.jpg"><img class="alignnone size-large wp-image-346" src="/images/posts/IMG_0174-1024x683.jpg" alt="IMG_0174" width="584" height="390" /></a></p>
<p>In deze grote grip lock zak vinden we volgende items;</p>
<ul>
<li>1x main PCB</li>
<li>1x PCB met LCD scherm</li>
<li>1 zakje met&nbsp;de nodige onderdelen (zoals condensators, weerstanden etc. etc.)</li>
<li>1x BNC -> 2x krokodillenbekjes</li>
<li>Handleidingen en schema's</li>
</ul>
<p><a href="/images/posts/IMG_0200.jpg"><img class="alignleft wp-image-350 size-thumbnail" src="/images/posts/IMG_0200-150x150.jpg" alt="IMG_0200" width="150" height="150" /></a>
<a href="/images/posts/IMG_0202.jpg"><img class="alignleft wp-image-351 size-thumbnail" src="/images/posts/IMG_0202-150x150.jpg" alt="IMG_0202" width="150" height="150" /></a> <a href="/images/posts/IMG_0217.jpg"><img class=" wp-image-352 size-thumbnail alignleft" src="/images/posts/IMG_0217-150x150.jpg" alt="IMG_0217" width="150" height="150" /></a> <a href="/images/posts/IMG_0243.jpg"><img class="alignleft wp-image-355 size-thumbnail" src="/images/posts/IMG_0243-150x150.jpg" alt="IMG_0243" width="150" height="150" /></a> <a href="/images/posts/IMG_0244.jpg"><img class="alignleft wp-image-356 size-thumbnail" src="/images/posts/IMG_0244-150x150.jpg" alt="IMG_0244" width="150" height="150" /></a>
<a href="/images/posts/IMG_0193.jpg"><img class="alignleft size-thumbnail wp-image-348" src="/images/posts/IMG_0193-150x150.jpg" alt="IMG_0193" width="150" height="150" /></a></p>
<p style="text-align: left;">Zoals je&nbsp;kan zien in de bovenstaande foto's, krijg je alles er bij wat nodig is om deze oscilloscoop kit in elkaar te zetten. Het enige wat je hierna zelf nog moet regelen is een voeding voor de oscilloscoop. Maar voor dat ik dat ga doen, eerst nog even een overzicht van de specificaties van de kit.</p>
<h3>SPECIFICATIES:</h3>

| Aantal kanalen | 1 |
| Accuraatheid | 12Bit |
| Processor | Cortex-M3 ARM (STM32F103C8) |
| Maximale sample rate | 1 Miljoen samples per seconde |
| Analoge bandbreedte | 200 Khz |
| Input impedantie | 1 Miljoen&nbsp;&Omega; |
| Maximum input voltage | 50 Volt peak-to-peak |
| Coupling modes | DC / AC / GND |
| Trigger | rising / falling edge |
| Input voltage | 8 - 12&nbsp;Volt 120 mA |
| Scherm resolutie | 2.4" 320 x 240 pixels TFT met 262.000 kleuren |
| Formaat | 117 x 76 x 15 mm (LxBxH) |
| Gewicht | 70 gram (exclusief kabels) |

<p>Qua&nbsp;prijs/kwaliteits verhouding vind ik het een heel erg interessante oscilloscoop, vooral omdat ik zelf al een tijdje opzoek was naar een betaalbare oscilloscoop. Vooral omdat ik op dit moment ook bezig met mijn domotica project&nbsp;en daarbij is een oscilloscoop toch wel een handig stuk gereedschap om eventuele fouten te kunnen opsporen, maar tot op heden&nbsp;waren alle oscilloscopen voor mij "onbetaalbaar" (in ieder geval duurder dan dat ik gemiddeld voor wou betalen)</p>
<p>Maar omdat het een soldeerkit is, moet het eerst in elkaar gezet worden. Je krijgt de handleidingen geprint bij de gehele kit, maar ze kan ze ook digitaal&nbsp;<a href="https://jyetech.com/Products/LcdScope/UserManual_138.pdf" target="_blank" data-proofer-ignore>bekijken</a> via&nbsp;de website van JYE Tech. Dan de assemblage nu eindelijk echt beginnen. Ik heb van mijn assemblage een time-lapse video gemaakt.</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/XZS9VU-zTPg" frameborder="0" allowfullscreen></iframe>
<p>Toen ik eenmaal klaar was, zag mijn handleiding er zo uit;<br />
<a href="/images/posts/IMG_0302.jpg"><img class="alignnone size-medium wp-image-395" src="/images/posts/IMG_0302-300x200.jpg" alt="IMG_0302" width="300" height="200" /></a></p>
<p>Het assembleren van de gehele kit, ging allemaal vrij vlekkeloos. Het enige waar ik even "moeite" mee had, waren&nbsp;de kleine 0805 SMD weerstanden (zie onderstaande foto, de 3.5mm jack is vergelijkingsmateriaal). Maar met wat geduld en een fatsoenlijke pincet kom je een heel eind :D<br />
<a href="/images/posts/IMG_0241.jpg"><img class=" wp-image-354 size-medium alignnone" src="/images/posts/IMG_0241-200x300.jpg" alt="IMG_0241" width="200" height="300" /></a></p>
<p>Toen ik dat eenmaal gehad had, was het ergste eigenlijk wel voorbij en&nbsp;kon ik alle through-hole onderdelen gaan solderen. Uiteindelijk was ik ongeveer 2 uur bezig met alles te assembleren. Zodra het assembleren gedaan is wordt het tijd om de DSO 138 te gaan kalibreren. Dat doe je heel simpel door de positieve krokodilbek vast te maken aan de ingebouwde squarewave&nbsp;generator (3.3 volt met 1 Khz) en vervolgens de trimmercondensatoren (CC4 en C6) bij te stellen te stellen met een schroevendraaier zoals beschreven staat in de handleiding.</p>
<p><a href="/images/posts/IMG_0256.jpg"><img class=" size-medium wp-image-381 alignnone" src="/images/posts/IMG_0256-300x200.jpg" alt="IMG_0256" width="300" height="200" /></a></p>
<p>Als de DSO 138 opstart krijgen we de volgende 2 schermen te zien. Het eerste scherm is de firmware versie die ge&iuml;nstalleerd is (die op mijn unit, nieuwer is dan de laatste die op de website van JYE Tech staat) en als 2de krijgen we het logo van JYE Tech te zien.</p>
<p><a href="/images/posts/IMG_0291.jpg"><img class="alignleft wp-image-392 size-thumbnail" src="/images/posts/IMG_0291-150x150.jpg" alt="IMG_0291" width="150" height="150" /></a><a href="/images/posts/IMG_0301.jpg"><img class="alignnone wp-image-393 size-thumbnail" src="/images/posts/IMG_0301-150x150.jpg" alt="IMG_0301" width="150" height="150" /></a></p>
<p>Zodra dat gedaan is, is de DSO 138 klaar voor gebruik! Dan wordt het tijd dat ik hem maar eens op de pijnbank leg!</p>
<p>Allereerst het scherm. Het scherm is met zijn 2.4 inch, adequaat waar hij dienst voor moet doen. Het is zeker niet het helderste of het beste scherm wat ik ooit heb gezien maar het voldoet. Daar in tegen is het natuurlijk ook geen scherm om foto's of complete series op te kijken. Ik heb hem even vergeleken met mijn eigen telefoon (Xiaomi Mi2S) en&nbsp;ik moet het de helderheid op het scherm van mijn Mi2S op 50% instellen wil het ongeveer overeenkomen (blote oog calibratie ;) ).&nbsp;De kijkhoeken zijn goed en het scherm is inprincipe vanuit alle kijkhoeken goed zichtbaar.</p>
<p><a href="/images/posts/IMG_0265.jpg"><img class="alignnone size-medium wp-image-384" src="/images/posts/IMG_0265-300x200.jpg" alt="IMG_0265" width="300" height="200" /></a></p>
<p>Nu komen we op het punt bediening. Hier&nbsp;valt voor JYE Tech nog wel wat puntjes voor te verdienen. Al liggen&nbsp;gelukkig al deze puntjes wel in de software.</p>
<p><a href="/images/posts/IMG_0266.jpg"><img class="alignleft size-thumbnail wp-image-385" src="/images/posts/IMG_0266-150x150.jpg" alt="IMG_0266" width="150" height="150" /></a><a href="/images/posts/IMG_0267.jpg"><img class="alignnone wp-image-386 size-thumbnail" src="/images/posts/IMG_0267-150x150.jpg" alt="IMG_0267" width="150" height="150" /></a></p>
<p>&nbsp;</p>
<p>Aan de linker kant van de PCB vinden wij 3 schakelaars. Waarmee de coupling, input gevoeligheid (en tegelijkertijd hoeveel Volt per divisie het is op het scherm) en hoeveel X de input is (dus 1X, 2X of 5X). Aan de rechterkant van de PCB vinden we 4 knoppen. De knoppen 'OK', '+', '-' en 'SEL'. Met de knop 'SEL' kunnen we door alle instelbare functies heen schakelen zoals de timebase, trigger methode (rising of falling edge), trigger mode en horizontale en verticale as. Met de plus en min knoppen kan je de waarde van de huidige waarde veranderen.<br />
Als je de knop 'OK' voor 1&nbsp;seconde ingedrukt houdt, zal de oscilloscoop in hold modus gaan waardoor de huidige waveform&nbsp;op het scherm vast gehouden zal worden tot dat je weer op 'OK' drukt. Hou je de 'OK' knop voor 3 seconde ingedrukt, dan zal de oscilloscoop een overlay laten zien met wat details over de huidige waveform zoals de frequentie, arbeidscyclus (duty cycle), Vmin, Vmax en nog wat andere voltage relateerde informatie.</p>
<p><a href="/images/posts/IMG_0261.jpg"><img class=" size-thumbnail wp-image-383 alignnone" src="/images/posts/IMG_0261-150x150.jpg" alt="IMG_0261" width="150" height="150" /></a></p>
<p>Met die overlay functie,&nbsp;dat is ook gelijk het&nbsp;grootste software hekelpunt. Op het moment dat je de 'OK' knop indrukt omdat je de overlay wilt zien, schiet de oscilloscoop gelijk ook in de hold modus. Dus moet je na dat je de overlay zichtbaar hebt, nogmaals de 'Ok' knop indrukken om de oscilloscoop vervolgens uit de "hold" modus te krijgen.</p>
<p>Daarnaast is het instellen van de triggerleven redelijk onhandig. Je moet eerst met de "SEL" knop naar het roze pijltje (dat is je triggerlevel) en kan je met het plus en min knop het triggerlevel naar boven of naar beneden instellen. Alleen gaat het omhoog en naar beneden bewegen van het triggerlevel echt met een slakkengang. Ik moest ongeveer 30 seconde de min knop gedrukt blijven houden om het trigger level van ongeveer 75% naar 10% te krijgen.</p>
<p>Ook&nbsp;heeft de DSO 138 nog een USB poort (USB mini) en een TTL interface. De TTL interface is eigenlijk bedoeld voor het upgraden van de firmware. Alleen&nbsp;zoals we in de <a href="https://jyetech.com/Products/LcdScope/DSO138_oscilloscope_upgrade.pdf" target="_blank" data-proofer-ignore>handleiding</a> kunnen lezen moeten we eerst JP1 en JP2 met elkaar verbinden en kunnen we&nbsp;dan vervolgens een USB -> TTL adapter aansluiten op de TTL header pins. Ik had het een mooiere oplossing gevonden als ze een extra schakelaar hadden toegevoegd waarmee je dan JP1 en JP2 verbond.<br />
Het nut van de USB poort heb ik nog niet gevonden en het staat ook niet vermeld in de handleiding. Misschien dat hier toekomstige software updates wat mee gedaan zal worden.</p>
<p>Dan komt nu de grote vraag van allemaal. Hoe presteert de DSO 138 ? Nou, ik heb zelf even een kleine "test opstelling" gemaakt met mijn telefoon met daarop een <a href="https://play.google.com/store/apps/details?id=com.tronotech.waveformgeneratorlite" target="_blank">waveform generator app</a>. Hiervoor heb ik de "Waveform Generator" app van Trono gebruikt. Vervolgens heb ik een standaard 3.5mm jack -> 3.5mm jack kabel gepakt om de output van de app (wat via de koptelefoon uitgang gaat) en daarop heb ik mijn oscilloscoop aangesloten en parallel daaraan heb ik ook nog eens mijn multimeter aangesloten omdat op mijn multimeter ook een hertz meet functie zit. Daar heb ik vervolgens wat standaard golven uit laten komen zoals een zaagtand, sinus en PWM golf.</p>
<p><a href="/images/posts/IMG_0274.jpg"><img class="alignleft size-thumbnail wp-image-388" src="/images/posts/IMG_0274-150x150.jpg" alt="IMG_0274" width="150" height="150" /></a><a href="/images/posts/IMG_0275.jpg"><img class=" size-thumbnail wp-image-389 alignleft" src="/images/posts/IMG_0275-150x150.jpg" alt="IMG_0275" width="150" height="150" /></a><a href="/images/posts/IMG_0282.jpg"><img class="alignleft size-thumbnail wp-image-390" src="/images/posts/IMG_0282-150x150.jpg" alt="IMG_0282" width="150" height="150" /></a><a href="/images/posts/IMG_0285.jpg"><img class=" size-thumbnail wp-image-391 alignnone" src="/images/posts/IMG_0285-150x150.jpg" alt="IMG_0285" width="150" height="150" /></a></p>
<p>En wat blijkt, tot mijn eigen grote verbazing. De DSO 138 blijkt zelfs nog redelijk accuraat te zijn ook. Er zit wel wat afwijking in en ruis in de aflezing, maar dat durf ik op dit moment nog niet af te schrijven als 'ontwerpfout' of beperking van dit ontwerp. Ik heb namelijk op het forum van JYE Tech meerdere threads&nbsp;gezien van mensen, dat zodra ze de LCD verder verplaatste van de main PCB dat de ruis op de aflezing minder aanwezig was. Daarnaast zie ik het ook nog wel gebeuren dat mijn gekozen voeding (een willekeurige 9 volt, 200 mA) die ik nog had liggen, ook nog wel&nbsp;wat roet in het eten zou kunnen gooien.</p>
<h3>Eindoordeel</h3>

| ✅ **Positief** | ❌ **Negatief** | 
| Goedkoop | 'Redelijk' ruizig | 
| Redelijk accuraat | Triggerlevel veranderen kan frustrerend zijn | 
| Fatsoenlijk scherm | Updaten firmware is nogal omslachtig | 
| Klein en compact | Geen (offici&euml;le) behuizing | 
| Bied mogelijkheden om via een batterij gevoed te worden | Geen voeding meegeleverd | 
| Open source firmware | 

<p></p>
<h3>Conclusie:</h3>
<p>Waar mee kan ik dit vergelijken ? En voor wie dit product nu uiteindelijk bedoeld ? Zou je het mensen aanraden ? Je zou het enigzins vergelijken met de Vellaman&nbsp;HPS140 ware het niet dat de Velleman&nbsp;HPS140 slechts een monochroom display heeft en een met zijn aanschafprijs van ongeveer 140 euro STUKKEN duurder is dan de DSO 138 met zijn ~ 21 euro. Dan nog de vraag over "Voor wie is nou eigenlijk bedoeld".&nbsp;De DSO 138 is perfect geschikt voor de beginnende elektronica hobbyist, die niet veel geld wil (of kan)&nbsp;uitgeven aan een oscilloscoop en toch ergens mee wilt beginnen&nbsp;of voor iemand die wilt weten hoe de interne werking&nbsp;van een oscilloscoop in elkaar steekt. Het gene wat hier meeste&nbsp;bij meedraagt is de opensource firmware van de DSO 138. En zou&nbsp;ik de DSO 138 aanraden aan mensen. Als je kan leven met de beperkingen en quirks die de DSO 138 meebrengt, zoals de beperkte bandbreedte van 200 Khz. Dan is het antwoord daarop een volmondige JA!</p>
<p>Mocht je nou denken,&nbsp;ik wil ook zo'n DSO 138 kopen. Hij op dit moment via <a href="https://www.banggood.com/DIY-Digital-Oscilloscope-Kit-Electronic-Learning-Kit-p-969762.html?p=TZ081713301692015034" target="_blank">BangGood</a>&nbsp;leverbaar voor &euro; 20,85.</p>
