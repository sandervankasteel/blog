---
layout: post
status: publish
published: true
title: 'Home automation - #2 WiFi is in da house!'
author: Sander van Kasteel
date: '2015-04-21 12:20:47 +0200'
categories:
- Programmeren
- Hardware
- Electronics
- Arduino
- Python
tags: []
comments: []
---
<p>Hij is binnen gekomen! De ESP8266&nbsp;WiFi module is een aantal dagen geleden afgeleverd door de vriendelijke meneer van de PostNL.</p>
<p><a href="/images/posts/IMG_20150418_031941.jpg"><img class="alignnone size-large wp-image-238" src="/images/posts/IMG_20150418_031941-1024x768.jpg" alt="IMG_20150418_031941" width="584" height="438" /></a></p>
<p>De ESP8266&nbsp;is een hartstikke leuke chipset. In de basis is het een 32 bits microprocessor die een aantal netwerk functionaliteit voor zijn rekening neemt. Deze chip heeft de mogelijkheid om als accesspoint, als client of als beide. Daarnaast kan je&nbsp;met 3&nbsp;simpele commando's een TCP of UDP connectie opzetten, wat data verzenden en deze zelfde connectie weer afsluiten. Daarnaast&nbsp;heeft de chip ook nog eens de mogelijkheid om buiten UART ook nog eens kan communiceren over SPI. Het leukste van dit alles is dat de ESP8266&nbsp;bijna niks kost. Ik heb voor de mijne $3.04 inclusief verzendkosten vanuit China betaald. Omgerekend is dat nog geen 3 euro.</p>
<p>Dus dan wordt het maar eens tijd om te kijken hoe de module precies werkt. Laat ik als eerst maar eens even de nodige vertaalde&nbsp;<a href="https://nurdspace.nl/ESP8266#AT_Commands" target="_blank">documentatie</a> er bij pakken en de pinout er bij pakken. Aangezien de originele datasheet alleen in Chinees te krijgen is, ben ik echter aangewezen op slecht vertaalde documentatie :(</p>
<p><a href="/images/posts/esp8266-pinout.jpg"><img class="alignnone size-full wp-image-233" src="/images/posts/esp8266-pinout.jpg" alt="esp8266-pinout" width="467" height="500" /></a></p>
<p><!--more--></p>
<p>Om de module aan te sluiten moet je 3.3 volt op de VCC aansluiten, GND moet&nbsp;uiteraard op aarde / negatief aangesloten worden. URXD is de UART receive lijn en UTXD is de UART transmit lijn. Deze mogen aangesloten kruislings aangesloten worden. Dus ESP8266 URXD moet aan de TX van UART interface en de UTXD moet aan de RX van de UART interface. Daarnaast moet je niet vergeten om CH_PD aan VCC aan te sluiten anders wil de ESP8266 niet opstarten. Dat heeft mij namelijk een goed uur uitzoekwerk gekost ;) (lees: RTFM!)</p>
<p><a href="/images/posts/DSC_7840.jpg"><img class="alignnone size-large wp-image-241" src="/images/posts/DSC_7840-1024x685.jpg" alt="DSC_7840" width="584" height="391" /></a></p>
<p>Na dit leuke puzzeltje wordt het maar eens&nbsp;tijd om te gaan kijken of de ESP8266 ook echt werkt. Om te testen of dat zo was, heb ik de ESP8266 aangesloten op een simpele<br />
USB naar&nbsp;TTL interface gebasseerd op een&nbsp;PL2303 chip. Puur als test heb ik de ESP8266 mijn prive WiFi netwerk (genaamd "Starfleet") laten joinen. Wat je niet moet vergeten is dat de ESP8266 niet(!!) compatible is met een breadboard layout (zie foto hieronder).&nbsp;<a href="/images/posts/DSC_7826.jpg"><img class="alignnone size-large wp-image-255" src="/images/posts/DSC_7826-1024x685.jpg" alt="DSC_7826" width="584" height="391" /></a></p>
<p>Zoals je kan zien in de screenshots hieronder, heeft het redelijk wat moeite gekost om de ESP8266 module online te krijgen. Maar eerst even een kleine tabel waarin de meest gebruikte commando's kunnen bekijken (bron: <a href="http://nurdspace.nl/ESP8266" target="_blank">nurdspace</a>)&nbsp;.<a href="/images/posts/AT-commands.png"><img class="alignnone size-full wp-image-247" src="/images/posts/AT-commands.png" alt="AT-commands" width="1004" height="629" /></a></p>
<p>Hieronder zie je 2 screenshots waarin ik&nbsp;de ESP8266 verbinding heb laten leggen met mijn WiFi netwerk. Je ziet herin wel wat "ERROR" meldingen terug komen, ik vermoed dat dit gekomen is door het gebruik van backspace of dat ik te snel commando's wou doorgeven aan de module.&nbsp;<a href="/images/posts/Screen-Shot-2015-04-21-at-01.38.18.png"><img class="alignnone size-large wp-image-231" src="/images/posts/Screen-Shot-2015-04-21-at-01.38.18-1024x640.png" alt="Screen Shot 2015-04-21 at 01.38.18" width="584" height="365" /></a></p>
<p><a href="/images/posts/Screen-Shot-2015-04-21-at-01.38.21.png"><img class="alignnone size-large wp-image-230" src="/images/posts/Screen-Shot-2015-04-21-at-01.38.21-1024x640.png" alt="Screen Shot 2015-04-21 at 01.38.21" width="584" height="365" /></a></p>
<p>Maar uiteindelijk is het dan ook gelukt!</p>
<p><a href="/images/posts/Screen-Shot-2015-04-21-at-01.38.21.png"><img class="alignnone size-full wp-image-229" src="/images/posts/Screen-Shot-2015-04-21-at-12.14.04.png" alt="Screen Shot 2015-04-21 at 12.14.04" width="996" height="329" /></a></p>
<p>Nu duidelijk is dat de ESP8266 werkt, wordt het maar eens tijd om in een Arduino omgeving te gaan verwerken.&nbsp;Gezien de hoeveelheid werk die het kostte om via een USB naar TTL seriele interface de ESP8266 te laten werken, heb ik besloten op zoek te gaan naar een Arduino library. Het eisenpakket stelt allemaal niet zo heel erg veel voor. De library moest vooral klein, functioneel en duidelijk zijn. Na wat googlen en rondspeuren op Github kwam ik&nbsp;ESP8266wifi van ekstrand tegen op Github (<a href="https://github.com/ekstrand/ESP8266wifi" target="_blank">link</a>).</p>
<p>Maar voor dat ik aan het programmeren kan, zal ik als eerst de ESP8266 aan mijn Arduino moeten aansluiten. &nbsp;Hiervoor heb ik het volgende schema bedacht.&nbsp;<a href="/images/posts/fritzing_bb.jpg"><img class="alignnone size-large wp-image-253" src="/images/posts/fritzing_bb-816x1024.jpg" alt="fritzing_bb" width="584" height="733" /></a></p>
<p>Ik heb de RX (receive UART lijn)&nbsp;via een 74HC4050 (<a href="https://www.nxp.com/docs/en/data-sheet/74HC4050.pdf" target="_blank">datasheet</a>) aangesloten op de PWM pin 3 van de Arduino omdat de ESP8266 geen 5 volt tolerante inputs heeft. De TX (transmit&nbsp;UART lijn) zit direct op PWM pin 2 van de Arduino aangesloten.&nbsp;PWM pin 12 gebruik ik op een LED aan te sturen.</p>
<p><a href="/images/posts/DSC_7883.jpg"><img class="alignnone size-large wp-image-273" src="/images/posts/DSC_7883-1024x685.jpg" alt="DSC_7883" width="584" height="391" /></a></p>
<p>Toen dat klaar was begon ik te bedenken hoe ik de Arduino ging aansturen. Mijn eerste gedachten&nbsp;was om dit over HTTP te doen. De reden om het over&nbsp;HTTP te gaan doen, was heel simpel. Het is een bewezen protocol, ik kan heel simpel met iedere taal die ik zou willen HTTP requests kunnen afhandelen en ik dacht dat de Arduino wel krachtig genoeg zou zijn om dat soort verzoeken af te handelen.&nbsp;<a href="/images/posts/HTTP-request-1.png"><img class="alignnone size-large wp-image-256" src="/images/posts/HTTP-request-1-1024x639.png" alt="HTTP-request-1" width="584" height="364" /></a></p>
<p>Zoals je echter kan zien in het screenshot hierboven, ging dat hem niet worden. Wat ik ook probeerde, zoals minder vaak een status ophalen of de HTTP encoding op ANSI / ISO-8851 zetten. Het mocht allemaal&nbsp;niet baten, mijn data kwam corrupt aan bij mijn Arduino. Dan maar terug naar de tekentafel. Na wat denken, peinzen en nog meer hersen gekraak, kwam ik tot geen andere conclusie dan zelf een simpele TCP server te gaan implementeren en om de uitdaging nog leuker te maken, heb ik besloten dit te gaan doen in Python.</p>
<p>Op dit moment is de TCP server vrij simpel ge&iuml;mplementeerd en op basis van het voorbeeld wat ik vond in de Python documentatie. Hij ondersteund op dit moment alleen maar 1 commando en dat is "get light" en dan de naam van lamp waar je de status van wilt opvragen. Op dat moment wordt er instantie aangemaakt van de klasse "Light" en in zijn constructor wordt er 1 parameter meegegeven namelijk "lightname". Op het moment dat de functie getStatus() wordt aangeroepen van de klasse Light, wordt er een gelijknamig bestand geopend en wordt deze uitgelezen. In mijn opzet&nbsp;is dat de "kitchen" light. Vervolgens wordt het resultaat via een TCP connectie terug gezonden naar de Arduino. Op basis van dat resultaat zal de Arduino de LED aan of uit schakelen.</p>
<p>En nu komt&nbsp;het moment waarop ik&nbsp;daden bij mijn woorden zet. De source van de TCP server:</p>
<p>home-automation.py</p>
{% highlight python %}
{% raw %}
__author__ = 'Sander van Kasteel'
import SocketServer
import time
from Lights import Lights
class EchoRequestHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        data = self.request.recv(1024).strip('\r\n') # Get the data from the client and immediatly strip off \r\n chars
        now = time.strftime("%c")
        print "[" + now + "] " + "request for '" + data + "' from client "  + self.client_address[0]
        if(len(data) > 1):
            lightName = Lights(data.rsplit('get light ')[1])
            self.request.send(lightName.getStatus())
        else :
            self.request.send("false")
if __name__ == '__main__':
    import socket
    import threading
    # address = ('localhost', 8000) # let the kernel give us a port
    address = ('', 7000) # let the kernel give us a port
    server = SocketServer.TCPServer(address, EchoRequestHandler)
    ip, port = server.server_address # find out what port we were given
    t = threading.Thread(target=server.serve_forever)
    t.setDaemon(True) # don't hang on exit
    t.start()
    server.serve_forever()
{% endraw %}
{% endhighlight %}

<p>lights.py</p>

{% highlight python %}
{% raw %}

__author__ = 'Sander van Kasteel'
import io
class Lights:
    light = ''
    def __init__(self, lightname):
        self.light = lightname
        # self.getStatus(lightName)
        # return self
    def getStatus(self):
        try:
            file = io.open(self.light, 'rt')
            fileContents = file.read().strip('\n')
            if(fileContents == "true"):
                file.close()
                return "true"
            else:
                file.close()
                return "false"
        except IOError as e:
            print "Oops! The file for this light doesn't exist"
            print e.filename
            return "false"

{% endraw %}
{% endhighlight %}
<p>En&nbsp;dan de source van de Arduino client</p>

{% highlight python %}
{% raw %}

#include <ESP8266wifi.h>
#include <SoftwareSerial.h>
#define RST_pin 7 // RESET pin on the ESP8266
SoftwareSerial swSerial(2, 3);
int ledPin = 12; // pin to the LED
boolean newStatus;
boolean oldStatus = false; // default state is FALSE
String returnData;
ESP8266wifi wifi(swSerial, swSerial, RST_pin);
//ESP8266wifi wifi(swSerial, swSerial, RST_pin, Serial); // DEBUGGING!
void setup() {
  // put your setup code here, to run once
  pinMode(ledPin, OUTPUT); // setup ledPin as output pin
  swSerial.begin(9600);
  Serial.begin(9600);
  wifi.setTransportToTCP();
  wifi.endSendWithNewline(true);
  wifi.begin();
  wifi.connectToAP("Starfleet", "xxxxxx"); // WiFi name and password
}
void loop() {
  // put your main code here, to run repeatedly:
  if (!wifi.isStarted() || !wifi.isConnectedToAP())
  {
    Serial.println(F("Starting WiFi again..."));
    wifi.begin();
  }
  newStatus = getNewStatus();
  if (newStatus == true)
  {
    digitalWrite(ledPin, HIGH); // turn ON the LED
  } else {
    digitalWrite(ledPin, LOW); // turn OFF the LED
  }
}
boolean getNewStatus()
{
  boolean isConnected = wifi.connectToServer("10.13.37.172", "7000");
  if (wifi.isConnectedToServer())
  {
    wifi.send(SERVER, "get light kitchen");
    WifiMessage in = wifi.listenForIncomingMessage(1000);
    if (in.hasData)
    {
        returnData = in.message;
    }
    Serial.println("Return data is " + returnData);
    if(returnData == "true")
    {
      // If the returnData is TRUE
      oldStatus = true;
      return true;
    } else if (returnData == "false") {
      // If the returnData is FALSE
      oldStatus = false;
      return false;
    } else {
       // incase of data corruption or some other error, then return "oldStatus"
       return oldStatus;
    }
  } else {
    // if there is no connection to the server, then return oldStatus
    return oldStatus;
  }
}

{% endraw %}
{% endhighlight %}

<p>En uiteraard hoort daar een ook een video bij hoe het er in het echt uit ziet.</p>
<iframe src="https://www.youtube.com/embed/gu3SnswemMU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<p>Al met al is dit geen super nette implementatie, maar het voldoet voor het moment. 1 van de features die ik nog wil toevoegen is logging. Daarnaast moet er nog een complete webinterface bij komen en&nbsp;eigenlijk moet de huidige "bestanden" bron vervangen door een (No)SQL database. Zodat ik ook nog wat meta data kan gaan opslaan&nbsp;zoals de tijden waarop een lamp aan en uitgeschakeld wordt. En zoals je in de video kan zien zit er soms wat vertraging tussen de wijziging van de file en de werkelijke verandering van de LED.</p>
<p>Er is dus nog genoeg te doen ;)</p>
