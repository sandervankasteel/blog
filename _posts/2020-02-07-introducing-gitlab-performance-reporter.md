---
layout: post
status: publish
published: true
background: '/images/bg-gpr.jpg'
title: Introducing Gitlab Performance Reporter
author: Sander van Kasteel
date: '2020-02-07 12:00:20 +0100'
date_gmt: '2020-02-07 13:00:20 +0100'
categories:
- Gitlab
- CI/CD
tags: []
---
## What is Gitlab Performance Reporter ?
Gitlab Performance Reporter, is a tool I wrote to fit one of my own needs. For my blog and other static websites I maintain, I wanted to be able to track what the performance impact certain commits and MRs had or has.

"That's all very nice, but doesn't Gitlab has that natively ?", I hear you ask. Yes it does, with one MAJOR catch. You need to be a 'Silver' or 'Premium' subscriber with Gitlab.
For a lone developer like me, costs a pretty penny ($19 a month). Especially if you take into consideration, that I don't use the rest of the functionality since I am a 'lone' developer.
The rest of the functionality of Gitlab 'Silver' / 'Premium' all revolves around team functionalities, which I am clearly not.

So that's why I made "Gitlab Performance Reporter". It allows me to replicate 0.001% of functionality, which Gitlab 'Silver' / 'Premium' offers which is interesting to me personally without spending $19 a month, for just 0.001% of functionalty.

## Technical aspects / architecture
It's is written in Python 3.8 and is being offered for use in a Gitlab CI/CD pipeline by using Docker. Gitlab by default integrates with sitespeed.io for "Browser Performance Testing" and that is what I am trying to mimick here. 

This whole application revolves around the comparing the output of the 'sitespeed.io' docker container (called `performance.json`) with output of a previous run. 
In the context of an Merge Request, this is comparing the `perfomance.json` of the target branch with the `performance.json` of the source branch. That difference in perfomance is then being relayed as a comment (notes in Gitlab API speak) on the Merge Request.

A preview screenshot of the functionality can be found at the end of this blogpost or a real life you can look at the [MR which made this blogpost possible](https://gitlab.com/sandervankasteel/blog/-/merge_requests/45){:target="_blank"}

Nice little detail, it is licensed under the MIT license ;-)

## Usage
"Talk is cheap, show me the integration".

Luckely it's quite simple, all you need to do the following, implement everything according to [Browser Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html){:target="_blank"} guide by Gitlab, generate a personal token and add the following snippet to your `.gitlab-ci.yaml` file and that's it!

```yaml
performance_reporter:
  stage: performance_report
  image: sandervankasteel/gitlab-performance-reporter:1.0.1
  dependencies:
    - performance
  script:
    - python /app/src/main.py --token $GITLAB_ACCESS_TOKEN
```

For a more in-depth guide checkout the [readme.md](https://gitlab.com/sandervankasteel/gitlab-performance-reporter/blob/master/README.md){:target="_blank"}

A full example of this can be found [in the gitlab-ci.yml of my blog](https://gitlab.com/sandervankasteel/blog/-/blob/develop/.gitlab-ci.yml){:target="_blank"}, please note that I use Netlify to deploy to and use the 'deploy previews' functionality to have a live preview and a benchmarkable environment to use this on.

## What's possibly next?
- Integration with [Lighthouse-ci](https://github.com/GoogleChrome/lighthouse-ci/blob/master/docs/getting-started.md){:target="_blank"} and possible others
- Configurable non-zero exit codes based on the percentage of failed metrics
- Allow external markdown templates to be used

## Screenshot
![Screenshot comment MR](https://gitlab.com/sandervankasteel/gitlab-performance-reporter/raw/master/docs/img/example-1.png)

## Support

Should run into any issues (since it's currently at version 1.0.1), don't hesitate to create an [issue](https://gitlab.com/sandervankasteel/gitlab-performance-reporter/issues){:target="_blank"} or have the Tweet birds do their thing through <a href="https://twitter.com/Hertog6" data-proofer-ignore target="_blank">Twitter</a>

## Links
- [Source](https://gitlab.com/sandervankasteel/gitlab-performance-reporter){:target="_blank"}
- [Docker Image](https://hub.docker.com/r/sandervankasteel/gitlab-performance-reporter){:target="_blank"}

## Notes
Please note, this project is in **no way** affiliated with Gitlab, GitLab Inc or it's subsidiaries.

The header image is made by Chris Reid and is available on [Unsplash](https://unsplash.com/photos/ieic5Tq8YMk){:target="_blank"}.
