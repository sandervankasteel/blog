---
layout: post
status: publish
published: true
title: OUYA spielerij
author: Sander van Kasteel
date: '2016-03-12 17:00:20 +0100'
date_gmt: '2016-03-12 16:00:20 +0100'
categories:
- Hacking
- Hardware
- Ouya
tags: []
comments: []
---
<p>Een tijdje terug heb ik via de V&amp;A van <a href="https://www.tweakers.net" target="_blank">Tweakers</a>&nbsp;een OUYA&nbsp;voor een leuk bedrag op de kop getikt. Leuk dingetje, kan er prima een paar Android games op spelen en daar kan ik prima wat&nbsp;avonduurtjes mee vullen. Maar dit is geen review van de OUYA, daar zijn er al genoeg van op het internet. Deze blogpost gaat&nbsp;meer over&nbsp;mijn speelavontuurtjes met de OUYA.</p>
<p>
    <img class="img-fluid" src="/images/posts/ouya-298x300.jpg" alt="ouya" width="298" height="300" />
</p>
<p>Mijn eerste punt om de OUYA meer te laten doen dan alleen wat games is het installeren van Kodi. De laatste versie van Kodi (wat vroeger XMBC heette) die in de store van OUYA staat is 12.2 en de laatste versie van Kodi die zonder alternatieve&nbsp;ROMs te installeren is, is Kodi v14.</p>
<p>Maar met Kodi v14 neem ik geen genoeg! Ik wil bleeding-edge want ja je bent een Tweaker of niet toch?&nbsp;;) Dus wil ik Kodi v16 (Jarvis) draaien maar voor dat we Kodi v16 kunnen installeren moeten&nbsp;we de OUYA wel even wat 'liefde' geven zodat&nbsp;we op zijn minst Android 4.2&nbsp;draaien. Omdat Kodi v16 op zijn minst Android KitKat vereist. Maar met de laatste update vanuit Ouya&nbsp;draaien we 'slechts' Android 4.1.2 (JellyBean).</p>
<p><img class="size-medium wp-image-533 alignnone" src="/images/posts/IMG_1459-200x300.jpg" alt="IMG_1459" width="200" height="300" /></p>
<p>Dus om te updaten naar 4.4 zullen we zelf even aan de slag moeten. Gelukkig bestaat er een&nbsp;kant en klare CyanogenMod image voor de OUYA. Uiteraard bestaat hiervoor de gebruikelijke waarschuwing.&nbsp;Ik ben niet aansprakelijk&nbsp;voor&nbsp;het bricken van je OUYA,&nbsp;het ontploffen van je huis of je kat / vriend / vriendin / SO die er spontaan vandoor gaat omdat je niks anders meer doet dan films en series kijken via je OUYA!</p>
<p>Voor dat we gaan beginnen heb je de volgende dingen nodig:</p>
<ul>
<li>USB naar microUSB kabel</li>
<li>USB toetsenbord</li>
<li>USB muis (optioneel)</li>
<li>USB hub (optioneel)</li>
</ul>
<p>Als vervanger voor de USB toetsenbord, muis en hub heb ik een Logitech K400 gebruikt. Makkelijk toetsenbordje inclusief touchpad zodat ik geen USB&nbsp;muis&nbsp;en&nbsp;hub&nbsp;nodig heb.</p>
<h2>Stap 1: Installeren van ADB</h2>
<hr />
<p>Voor dat we kunnen beginnen met het installeren van CM11, moeten we de nodige Android-tools op onze computer&nbsp;installeren. Daarvoor moeten we Android Studio installeren zodat we onder Windows / Linux / OS X de nodige software en drivers (in het geval van Windows en OS X) krijgen. Laten we allereerst beginnen met Android Studio. De&nbsp;installatie bestanden hiervoor&nbsp;kan je op de <a href="https://developer.android.com/studio" target="_blank" data-proofer-ignore>Android Studio website</a> vinden. Mocht je&nbsp;Arch Linux draaien, dan kan je Android Studio zelfs direct uit de <a href="https://aur.archlinux.org/packages/android-studio" target="_blank">AUR</a>&nbsp;downloaden. Voor de overige Linux distro's zal je het moeten doen met de binary die Google levert.</p>
<p>Zodra we Android Studio ge&iuml;nstalleerd hebben, kunnen we Android Studio opstarten en dan komen we in het volgende scherm terecht.</p>
<p><a href="/images/posts/android-sdk0.png"><img class="size-medium wp-image-492 alignnone" src="/images/posts/android-sdk0-300x241.png" alt="android-sdk0" width="300" height="241" /></a></p>
<p>Daar kiezen we vervolgens voor de optie &ldquo;Configure&rdquo; en dan voor de optie <em>Android SDK Manager</em></p>
<p><a href="/images/posts/android-sdk.png"><img class="size-medium wp-image-494 alignnone" src="/images/posts/android-sdk-300x241.png" alt="android-sdk" width="300" height="241" /></a></p>
<p>Zodra we daarvoor hebben gekozen, kunnen we alle nodige packages kiezen en installeren. We hebben de volgende packages nodig <em>Android 4.1.2 (API 16): SDK Platform</em>, <em>Android Support Library</em> en als je&nbsp;Windows of OS X draait de <em>Google USB Driver</em>.</p>
<p><a href="/images/posts/android-sdk2.png"><img class="size-medium wp-image-497 alignnone" src="/images/posts/android-sdk2-300x202.png" alt="android-sdk2" width="300" height="202" /></a></p>
<p><a href="/images/posts/android-sdk3.png"><img class="size-medium wp-image-498 alignnone" src="/images/posts/android-sdk3-300x202.png" alt="android-sdk3" width="300" height="202" /></a></p>
<p><a href="/images/posts/android-sdk4.png"><img class=" size-medium wp-image-499 alignnone" src="/images/posts/android-sdk4-300x187.png" alt="android-sdk4" width="300" height="187" /></a></p>
<p>De Linux gebruikers zijn nu klaar. De Windows en OS X gebruikers zullen nog wat extra's&nbsp;moeten doen.&nbsp;De Windows gebruikers zal&nbsp;onder Windows 8 (en hoger) zullen driver signature verification moeten uitzetten en &eacute;&eacute;n wijziging moeten doorvoeren in de "android_winusb.inf" file. <a href="https://github.com/ouya/docs/blob/master/setup.md#windows-1" target="_blank">Meer info voor Windows</a>. OS X gebruikers hoeven alleen maar het installatiepad van de Android Studio toe te voegen aan hun $PATH. <a href="https://github.com/ouya/docs/blob/master/setup.md#mac-os-x-1" target="_blank">Voor meer informatie voor OSX</a>.</p>
<p>Nu hoeven we alleen nog maar &eacute;&eacute;n ding in te stellen op de OUYA&nbsp;en dat is, ADB aanzetten. Dat doen we door naar Manage -> System -> Development. Daar kunnen we vervolgens ADB aan / uit (on / off) zetten. Daarna kunnen we de OUYA via de micro USB kabel aansluiten aan onze compter.</p>
<p><a href="/images/posts/IMG_1450.jpg" rel="attachment wp-att-532"><img class="size-medium wp-image-532 alignnone" src="/images/posts/IMG_1450-200x300.jpg" alt="IMG_1450" width="200" height="300" /></a></p>
<p>Zodra we dat gedaan hebben kunnen de output van "adb devices" in een terminal (of adb.exe devices in een commandprompt voor Windows gebruikers) gebruiken om te kijken of de OUYA goed gedeceteert wordt. Als het allemaal goed gegaan is krijgen we ongeveer de volgende output;</p>
<p><a href="/images/posts/output-adb.png" rel="attachment wp-att-506"><img class=" size-medium wp-image-506 alignnone" src="/images/posts/output-adb-300x149.png" alt="output-adb" width="300" height="149" /></a></p>
<h2>Stap 2: Installeren van CWM (ClockWorkMod)</h2>
<hr />
<p>Als dat allemaal goed is gegaan, kunnen we gaan beginnen met het installeren van CWM.&nbsp;Dit lijkt allemaal vrij indrukwekkend en moeilijk maar valt allemaal reuze mee :) Voor dat we beginnen hebben we de <a href="/downloads/posts/cwm.zip" rel="">volgende bestanden</a> nodig en&nbsp;moeten we even de USB toetsenbord, muis en hub aansluiten (in mijn geval een K400). Ik heb dit bestand voor het gemak in&nbsp;"/home/sander/tmp/ouya/" uitgepakt.</p>
<p>Let op! Bluetooth toetsenborden en muizen zullen niet werken in de bootloader.</p>
<p>Als eerst moeten we de nodige bestanden via adb "pushen" naar de "sdcard" van de OUYA. SDcard is bewust in aanhalingstekens gezet, omdat de OUYA&nbsp;geen echte sdcard of sdcard slot heeft, maar een partitie is op het flash geheugen van de OUYA. Dit doen we met het volgende commando:</p>
<p><strong>OS X en Linux gebruikers:</strong></p>
<pre>adb push /home/sander/tmp/ouya/recovery-clockwork-6.0.4.8-ouya.zip /sdcard/recovery-clockwork-6.0.4.8-ouya.zip</pre>
<p style="text-align: justify;"><strong>Windows gebruikers:</strong></p>
<pre style="text-align: justify;">adb.exe push C:\Users\username\Desktop\recovery-clockwork-6.0.4.8-ouya.zip /sdcard/recovery-clockwork-6.0.4.8-ouya.zip</pre>
<p style="text-align: justify;">Zodra dat klaar is, krijgen we het volgende te zien :<br />
<a href="/images/posts/adb-push-done.png" rel="attachment wp-att-515"><img class="size-medium wp-image-515" src="/images/posts/adb-push-done-300x18.png" alt="adb-push-done" width="300" height="18" /></a></p>
<p>Dan gaan we via ADB zorgen dat de OUYA&nbsp;zich reboot&nbsp;naar de bootloader met;</p>
<p><strong>OS X en Linux gebruikers:</strong></p>
<pre>adb reboot bootloader</pre>
<p><strong>Windows gebruikers:</strong></p>
<pre>adb.exe reboot bootloader</pre>
<p>Nu moeten we ongeveer 30 seconden wachten voor dat we weer wat kunnen doen. Je krijgt namelijk geen enkele visuele feedback op het scherm dat er wat gebeurt is.</p>
<p>Daarna kunnen met fastboot&nbsp;zorgen dat de OUYA&nbsp;opnieuw wordt opgestart in recovery en&nbsp;dat er een specifieke bootimage wordt gebruikt bij het opstarten. Dit doen we met:</p>
<p><strong>OS X en Linux gebruikers:</strong></p>
<pre>fastboot boot /home/sander/tmp/ouya/recovery-clockwork-6.0.4.8-ouya.img</pre>
<p><strong>Windows gebruikers:</strong></p>
<pre>fastboot.exe boot c:\Users\username\Desktop\recovery-clockwork-6.0.4.8-ouya.img</pre>
<p>De Linux (en OS X) gebruikers onder ons kunnen de volgende melding krijgen :</p>
<pre>titan% fastboot boot /home/sander/tmp/ouya/recovery-clockwork-6.0.4.8-ouya.img
< waiting for device ></pre>
<p>Je zal dan fastboot moeten draaien via sudo om dat probleem te verhelpen.</p>
<p>Als&nbsp;alles goed is gegaan (wat meestal niet langer duurt dan 10 &aacute; 15 seconden), krijg je een CWM Recovery scherm voor je neus! Mocht je niet het CWM recovery scherm krijgen, herstart dan de OUYA&nbsp;en probeer het opnieuw vanaf "adb reboot bootloader"</p>
<p><a href="/images/posts/IMG_1469.jpg" rel="attachment wp-att-552"><img class="size-medium wp-image-552 alignnone" src="/images/posts/IMG_1469-300x200.jpg" alt="IMG_1469" width="300" height="200" /></a></p>
<p>Nu kunnen we de naar de "sdcard" gepushte zipfile gaan installeren. Met het toetsenbord selecteren we "Install zip" -> "Choose zip from /sdcard", daar kunnen we vervolgens "recovery-clockwork-6.0.4.8-ouya.zip" selecteren. Zodra we deze geselecteerd hebben, krijgen we een verificatie melding van CWM met de vraag of we zeker weten&nbsp;of we die zipfile willen installeren.</p>
<p><span style="font-weight: 300;"><a href="/images/posts/IMG_1462.jpg" rel="attachment wp-att-561"><img class="size-medium wp-image-561 alignnone" src="/images/posts/IMG_1462-300x200.jpg" alt="IMG_1462" width="300" height="200" /></a></span></p>
<p><span style="font-weight: 300;"> Als dit allemaal gedaan is, kunnen we terug naar het hoofdmenu via en kunnen we de OUYA&nbsp;opnieuw gaan opstarten via "reboot system now". &nbsp;Het kan zijn dat CWM vraagt om "ROM may flash stock recovery on boot. FIX?" kies dan voor "Yes".</span></p>
<h2>Optionele stap: Backup maken van je huidige OUYA&nbsp;installatie</h2>
<hr />
<p>Mocht je een backup willen maken van je huidige Android installatie is dit je kans! Dit kunnen we heel simpel via ADB doen met het volgende commando.</p>
<p><strong>OS X en Linux gebruikers:</strong></p>
<pre>titan% adb backup -f ouya-09-03-2016.ab -all</pre>
<p><strong>Windows gebruikers:</strong></p>
<pre>adb.exe backup -f ouya-09-03-2016.ab -all</pre>
<p>Zodra we dit hebben gedaan zal de OUYA&nbsp;vragen om verificatie van deze stap en vragen om een niet verplicht encryptie wachtwoord.</p>
<p>Als daar eenmaal een akkoord op is gegeven, zal er een backup gemaakt worden naar de computer waarop de Ouya is aangesloten.</p>
<p><a href="/images/posts/IMG_1458.jpg" rel="attachment wp-att-559"><img class=" size-medium wp-image-559 alignnone" src="/images/posts/IMG_1458-300x200.jpg" alt="IMG_1458" width="300" height="200" /></a></p>
<h2>Stap 3: Vervangen van het bootmenu</h2>
<hr />
<p>Mocht je niet weten wat een bootmenu is, een bootmenu is een eigenlijk niks meer dan een stukje software die het in ons geval mogelijk maakt om vanuit het opstarten, door te starten&nbsp;naar de recovery modus, bootloader of failsafe modus. Aangezien de OUYA buiten de powerbutton geen andere hardwarematige knoppen heeft, is dat best handig. Mocht er om wat voor reden dan ook het OS niet bootbaar blijkt te zijn, dan kunnen we via het bootmenu toch in de recovery modus komen en zo proberen het OS te herstellen.</p>
<p>Uit eindelijk komt het bootmenu er zo uit te zien:</p>
<p><a href="/images/posts/IMG_1495.jpg" rel="attachment wp-att-565"><img class="size-medium wp-image-565 alignnone" src="/images/posts/IMG_1495-200x300.jpg" alt="IMG_1495" width="200" height="300" /></a></p>
<p>Voor dat er begonnen kan worden met vervangen het bootmenu en het installeren van CM11 inclusief alle Gapps (Google Apps), zullen we eerst de eerst van de&nbsp;bestanden moeten <a href="/downloads/posts/cm11-apps-bootmenu-keys.zip" rel="">downloaden</a>.</p>
<p>Zodra&nbsp;we deze bestanden binnen gesleept hebben, kunnen we beginnen met het pushen van alle bestanden naar de "sdcard" van de OUYA&nbsp;met adb.</p>
<p style="text-align: justify;"><a href="/images/posts/adb-push-cm11.png" rel="attachment wp-att-521"><img class="size-medium wp-image-521" src="/images/posts/adb-push-cm11-300x61.png" alt="adb-push-cm11" width="300" height="61" /></a></p>
<p><strong>OS X en Linux gebruikers:</strong></p>
<pre>titan% adb push /home/sander/tmp/ouya/cm-11-20151221-UNOFFICIAL-ouya.zip /sdcard/cm-11-20151221-UNOFFICIAL-ouya.zip
1305 KB/s (203281107 bytes in 152.094s)
titan% adb push /home/sander/tmp/ouya/gapps-kk-20140105.zip /sdcard/gapps-kk-20140105.zip
1302 KB/s (87179530 bytes in 65.358s)
titan% adb push /home/sander/tmp/ouya/ouyabootmenu-mlq-v2.0.4.zip /sdcard/ouyabootmenu-mlq-v2.0.4.zip
1298 KB/s (7856281 bytes in 5.907s)
titan% adb push /home/sander/tmp/ouya/ouya-keylayouts.zip /sdcard/ouya-keylayouts.zip
1252 KB/s (181446 bytes in 0.141s)</pre>
<p><strong>Windows gebruikers:</strong></p>
<pre>adb.exe push c:\Users\username\Desktop\cm-11-20151221-UNOFFICIAL-ouya.zip /sdcard/cm-11-20151221-UNOFFICIAL-ouya.zip
1305 KB/s (203281107 bytes in 152.094s)
adb.exe push c:\Users\username\Desktop\gapps-kk-20140105.zip /sdcard/gapps-kk-20140105.zip
1302 KB/s (87179530 bytes in 65.358s)
adb.exe push c:\Users\username\Desktop\ouyabootmenu-mlq-v2.0.4.zip /sdcard/ouyabootmenu-mlq-v2.0.4.zip
1298 KB/s (7856281 bytes in 5.907s)
adb.exe push c:\Users\username\Desktop\ouya-keylayouts.zip /sdcard/ouya-keylayouts.zip
1252 KB/s (181446 bytes in 0.141s)</pre>
<p>Nu alle bestanden overgezet zijn, kunnen via "adb reboot recovery" de OUYA laten rebooten in recovery mode, zodat we weer&nbsp;terecht komen bij het CWM recovery scherm.&nbsp;Dan gaan we nu eerst het bootmenu vervangen. Dit doen we net zoals de vorige keer via "install zip" -> "choose zip from /sdcard" en dan kiezen we voor "ouyabootmenu-mlq-v2.0.4.zip". Als alles dan goed is gegaan krijgen we de volgende medeling van CWM:</p>
<p><a href="/images/posts/IMG_1461.jpg" rel="attachment wp-att-539"><img class="size-medium wp-image-539 alignnone" src="/images/posts/IMG_1461-300x200.jpg" alt="IMG_1461" width="300" height="200" /></a></p>
<p>Nu kunnen we verder met stap 4. Reboot de OUYA&nbsp;overigens nog niet!</p>
<h2>Stap 4: Installeren van CM11, Gapps en controller&nbsp;mappings</h2>
<hr />
<p>Dan komt nu het belangrijkste gedeelte het installeren van CyanogenMod 11, de nodige Gapps (Google Apps zoals de Playstore) en de controller mappings zodat we de OUYA&nbsp;controller (maar ook een&nbsp;PS3 of een Wii controller) kunnen gaan gebruiken. Laten we als eerst maar eens gaan beginnen met CM11. Dit is dezelfde procedure als de vorige keer, alleen dit keer kiezen we voor&nbsp;cm-11-20151221-UNOFFICIAL-ouya.zip bij "install zip" -> "choose zip from /sdcard".</p>
<p><a href="/images/posts/IMG_1464.jpg" rel="attachment wp-att-538"><img class="size-medium wp-image-538 alignnone" src="/images/posts/IMG_1464-300x200.jpg" alt="IMG_1464" width="300" height="200" /></a></p>
<p>Zodra dit klaar is, doen we het zelfde maar dan voor&nbsp;gapps-kk-20140105.zip en&nbsp;ouya-keylayouts.zip</p>
<p><a href="/images/posts/IMG_1467.jpg" rel="attachment wp-att-537"><img class="size-medium wp-image-537 alignnone" src="/images/posts/IMG_1467-300x200.jpg" alt="IMG_1467" width="300" height="200" /></a></p>
<p><a href="/images/posts/IMG_1468.jpg" rel="attachment wp-att-536"><img class="size-medium wp-image-536 alignnone" src="/images/posts/IMG_1468-300x200.jpg" alt="IMG_1468" width="300" height="200" /></a></p>
<p>Het enige wat ons nu nog rest te doen, is het leegmaken&nbsp;van de cache partitie, leegmaken van de Dalvik Cache en het leegmaken van de data partitie.&nbsp;Het leegmaken van de data partitie en de cache partitie kunnen we heel simpel doen vanuit het hoofd menu van CWM.</p>
<p><a href="/images/posts/IMG_1474.jpg" rel="attachment wp-att-555"><img class="size-medium wp-image-555 alignnone" src="/images/posts/IMG_1474-300x200.jpg" alt="IMG_1474" width="300" height="200" /></a></p>
<p>Voor het leegmaken van de Dalvik cache, moeten we het "advanced" menu openen en daar kunnen we vervolgens kiezen voor de optie "wipe dalvik cache".</p>
<p><a href="/images/posts/IMG_1475.jpg" rel="attachment wp-att-556"><img class="size-medium wp-image-556 alignnone" src="/images/posts/IMG_1475-300x200.jpg" alt="IMG_1475" width="300" height="200" /></a></p>
<p>Zodra dat eenmaal gedaan is,&nbsp;links onderaan het scherm de melding te zien "Dalvik Cache wiped"</p>
<p><a href="/images/posts/IMG_1477.jpg" rel="attachment wp-att-535"><img class=" size-medium wp-image-535 alignnone" src="/images/posts/IMG_1477-300x200.jpg" alt="IMG_1477" width="300" height="200" /></a></p>
<p>Het spannendste moment van alles, komt nu. Is alles&nbsp;goed gegaan? Nu gaan we terug naar het hoofdscherm CWM en gaan we de OUYA eens rebooten! Als alles goed is gegaan krijg je uiteindelijk de "first-run" wizard&nbsp;van CM11 te zien en kan je aan de slag met het configureren van je Android installatie.</p>
<p><a href="/images/posts/IMG_1482.jpg" rel="attachment wp-att-550"><img class=" size-medium wp-image-550 alignnone" src="/images/posts/IMG_1482-300x200.jpg" alt="IMG_1482" width="300" height="200" /></a></p>
<h2>Stap 5: Installeren van Kodi en koppelen van de&nbsp;OUYA controller</h2>
<hr />
<p>Zodra je eenmaal de first-run van CyanogenMod hebt doorlopen, rest ons alleen nog maar 2 zaken om te doen.&nbsp;Als eerst&nbsp;gaan we OUYA controller via Bluetooth koppelen met de OUYA. Dat doen we in het tabje "Bluetooth" van het instellingen.</p>
<p>Vervolgens moeten we een paar seconden de "U" knop (die zit tussen de D-Pad en de rechter analog stick) ingedrukt houden, tot dat de buitenste&nbsp;2 van de 4 witte leds op de controller branden. Daarna kan je hem selecteren, zodat die gekoppeld wordt.</p>
<p><a href="/images/posts/IMG_1775.jpg" rel="attachment wp-att-599"><img class="size-medium wp-image-599 alignnone" src="/images/posts/IMG_1775-300x200.jpg" alt="IMG_1775" width="300" height="200" /></a><br />
<a href="/images/posts/IMG_1487.jpg" rel="attachment wp-att-546"><img class="size-medium wp-image-546 alignnone" src="/images/posts/IMG_1487-300x200.jpg" alt="IMG_1487" width="300" height="200" /></a> <a href="/images/posts/IMG_1488.jpg" rel="attachment wp-att-547"><img class="size-medium wp-image-547 alignnone" src="/images/posts/IMG_1488-300x200.jpg" alt="IMG_1488" width="300" height="200" /></a></p>
<p>Nu kunnen we Kodi gaan installeren, daar hebben we 2 opties voor. We gebruiken de Google Play Store, waarmee we op dit moment Kodia v16.0 op de OUYA ge&iuml;nstalleerd krijgen of we installeren Kodi v16.1 RC 1 via een APK file die op de website van Kodi zelf staast. Zelf heb ik gekozen voor de versie uit de Google Playu Store maar mocht je Kodi v16.1 RC 1 willen draaien op je OUYA, dan kan je <a href="https://mirrors.kodi.tv/releases/android/arm/">hier</a> de APK file downloaden.</p>
<p><a href="/images/posts/IMG_1491.jpg" rel="attachment wp-att-548"><img class=" size-medium wp-image-548 alignnone" src="/images/posts/IMG_1491-300x200.jpg" alt="IMG_1491" width="300" height="200" /></a></p>
<h2>Stap 6:&nbsp;Enjoy!</h2>
<hr />
<p>Er rest nu nog maar 1 ding! Biertje pakken, wat snacks en genieten van je harde arbeid!</p>
<p><a href="/images/posts/IMG_1789.jpg" rel="attachment wp-att-598"><img class="size-medium wp-image-598 alignnone" src="/images/posts/IMG_1789-300x200.jpg" alt="IMG_1789" width="300" height="200" /></a></p>
<h2>Veel voorkomende 'problemen'</h2>
<hr />
<p>Mocht je tegen een aantal problemen lopen. Kan je 1 van de onderstaande oplossingen proberen, om te kijken of deze jou probleem oplost. Alle&nbsp;commando's moet je uitvoeren in de "terminal emulator" app. Let op, je moet wel rebooten&nbsp;voor dat deze wijzigingen van kracht worden.</p>
<ul>
<li>Mocht je de output van OUYA op 720p willen hebben.
<ul>
<li>
<pre>su
setprop persist.tegra.hdmi.resolution 720p</pre>
</li>
</ul>
</li>
<li>Mocht je last hebben van overscan. De waarde die je moet invullen bij "setprop persist.sys.hdmi.overscan.val" moet tussen de 0.0 en 0.2 liggen. Een waarde zoals 0.05&nbsp;wordt ook geaccepteerd. (Dit was de&nbsp;waarde die ik moest invullen om het beeld op mijn TV goed te krijgen)
<ul>
<li>
<pre>su
setprop persist.sys.hdmi.overscan 1 
setprop persist.sys.hdmi.overscan.val [0.0 - 0.2]</pre>
</li>
</ul>
</li>
<li>Mocht je ethernet verbinding niet werken.
<ul>
<li>
<pre>su
netcfg eth0 up
netcfg eth0 dhcp</pre>
</li>
</ul>
</li>
</ul>
<h2>Wat staat er nog meer op de planning voor 2016.. ?</h2>
<hr />
<p>In de planning staat nog op zijn de volgende 2 review items: Een review van een TP-Link SG105E, een reparatie video (jaja een echte, inclusief mijn gezicht en alles!) en nog een review van een mysterie pakketje uit China...</p>
<p><a href="/images/posts/CcA94JPWEAA1e1N.jpg" rel="attachment wp-att-609"><img class="alignleft size-medium wp-image-609" src="/images/posts/CcA94JPWEAA1e1N-300x184.jpg" alt="CcA94JPWEAA1e1N" width="300" height="184" /></a></p>
