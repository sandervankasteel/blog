---
layout: post
status: publish
published: true
title: Review of the Xiaomi Mijia Bedside Lamp 2
author: Sander van Kasteel
date: '2021-02-07 12:00:00 +0100'
date_gmt: '2021-02-07 12:00:00 +0100'
categories:
- Smart Home
---

A little over 6 months ago I took the jump and decided to move (yes during the first lockdown) to a bigger place.
If you have been following my blog, you will know that I previously lived in a 30m2 studio apartment. Right now, I live in a 3 bedroom upstairs apartment 🎉. Which means more space and opportunities for Home Automation, which is exactly I have been planning (and secretly doing).

One of those opportunities, are bedside lamps. Usually I would have gone to the IKEA, get something that I "liked" and call it a day. But not this time, this time I got something I actually liked. I got _two_ Xiaomi Mijia Bedside Lamp's 2 for my bedroom.

## Introduction

The Xiaomi Mijia Bedside Lamp, costs around €40 / $50 / £35 (depending on where you buy it). Which wouldn't usually be something I would spend on a nightlight, but the amount of features it offers definitely makes up for.

Besides being a 'regular' nightlight, it offers more options:

- 18 LEDS (12x 0.6 Watts and 6x 0.3 watt)
- Maximum brightness of 400 lumen
- It has full RGB support (16 million colours, CRI of RA80)
- Colour temperature control (from warm white to cold white, 1700k - 6500k)
- Can be setup as a 'wakeup' light
- Has WiFi (2,4 Ghz) and Bluetooth 4.2
- App control (Xiaomi Home)
    - Timers
    - 'Nightmode'
    - Schedules
- Google Assistant, Amazon Alexa and Apple HomeKit support
- Has an on-device brightness slider
- Can be linked up (through the Xiaomi Home app) with additional smart devices (like the Xiaomi Aqara-range of devices)

## Unboxing

The Xiaomi Mijia Bedside Lamp 2 comes in adequate packaging, nothing to fancy.

![Front of the Xiaomi Mijia Bedside Lamp 2 box](/images/posts/xiaomi_bedsidelamp_2_review/lamp_1.jpg){:class="img-fluid" width="300px" loading="lazy"}

![Side of the Xiaomi Mijia Bedside Lamp 2 box](/images/posts/xiaomi_bedsidelamp_2_review/lamp_2.jpg){:class="img-fluid" width="300px" loading="lazy"}

When we open up the box, the first thing we find the manual and some safety guidelines.

![Inside of the Xiaomi Mijia Bedside Lamp 2 box with manuals](/images/posts/xiaomi_bedsidelamp_2_review/lamp_3.jpg){:class="img-fluid" width="300px" loading="lazy"}

Below that, is another box containing the adapter with an exchangable power plug.

![Inside of the Xiaomi Mijia Bedside Lamp 2 box with where the adapter is](/images/posts/xiaomi_bedsidelamp_2_review/lamp_4.jpg){:class="img-fluid" width="300px" loading="lazy"}

![Xiaomi Mijia Bedside Lamp 2 power brick](/images/posts/xiaomi_bedsidelamp_2_review/lamp_7.jpg){:class="img-fluid" width="300px" loading="lazy"}

Then finally we find the Xiaomi Mijia Bedside Lamp 2 itself, wrapped in a cloth-like 'bag'

![The Xiaomi Mijia Bedside Lamp 2 inside the box](/images/posts/xiaomi_bedsidelamp_2_review/lamp_5.jpg){:class="img-fluid" width="300px" loading="lazy"}

And now (without further ado), the star of the show ✨✨

![The Xiaomi Mijia Bedside Lamp 2](/images/posts/xiaomi_bedsidelamp_2_review/lamp_6.jpg){:class="img-fluid" width="300px" loading="lazy"}


## First impressions

First impressions on the unit, the light itself has quite some weight to it. So it's not that you will knock it off your nightstand without some effort. The part of the unit that actually passes through the light is made of plastic, with what appears to be a diffuse coating on the inside. 
So any light that will pass through it, will presumably look very natural. On the bottom of the unit, we find some rubber feet with a sticker on the bottom containing the serial number, MAC address, rated power and the model number.

The unit itself is about 200mm in height (7.87") and 140mm across (5.51"). I use the term across, since the it's has the shape of cylinder. 

Apart from how the unit feels, it looks quite nice. It has a very minimalistic appearance and will look nicely in any bedroom interior. 

## Setting up the app

Setting up the unit including App is pretty simple. All you need to do is, download the [Xiaomi Home App (for Android)](https://play.google.com/store/apps/details?id=com.xiaomi.smarthome){:target="blank"} or [Xiaomi Home App (for iPhone)](https://apps.apple.com/us/app/mi-home-xiaomi-smarthome/id957323480){:target="blank"} (if you don't have it already). Once that is done, you can then register an account with Xiaomi, login and then you should be on the homescreen of the Xiaomi Home App.

When you are on the homepage, you can just press the "+" sign in the top right corner and the app will search for the Xiaomi Mijia Bedside Lamp 2. 

![Add new device to Xiaomi Home](/images/posts/xiaomi_bedsidelamp_2_review/add_device_1.png){:class="img-fluid" width="300px" loading="lazy"}

Once you pressed that, the app will start looking for the Xiaomi Mijia Bedside Lamp. When it finds that it will, it will appear on the top of the screen.

![Adding a new device to Xiaomi Home, step 2](/images/posts/xiaomi_bedsidelamp_2_review/add_device_2.png){:class="img-fluid" width="300px" loading="lazy"}

Then you click on the device you want to add (in my case the lamp called "*Mi Bedside Lamp 20f4e*"), after that,it will ask you which WiFi network it should connect to.

![Adding a new device to Xiaomi Home, step 3](/images/posts/xiaomi_bedsidelamp_2_review/add_device_3.png){:class="img-fluid" width="300px" loading="lazy"}

After that, it will save all the settings and you should get a successful "Device added" message.

![Adding a new device to Xiaomi Home, successfully added](/images/posts/xiaomi_bedsidelamp_2_review/add_device_4.png){:class="img-fluid" width="300px" loading="lazy"}

When you press next, you can add the device to a specific room (with the Xiaomi Home app) and optionally add it to your "favorites" in the Xiaomi Home app.

![Adding a new device to Xiaomi Home, add to room](/images/posts/xiaomi_bedsidelamp_2_review/add_device_5.png){:class="img-fluid" width="300px" loading="lazy"}

In the last step, you can rename the device to anything you want. I renamed it to "Bedside Lamp R", since it going on my right night stand.

![Adding a new device to Xiaomi Home, rename device](/images/posts/xiaomi_bedsidelamp_2_review/add_device_6.png){:class="img-fluid" width="300px" loading="lazy"}

## Demo app

Once the Xiaomi Mijia Bedside Lamp 2 has been setup, we can use the app to the control it. Once you selected your lamp, you will see a screen with 4 buttons. "on / off", "White", "Color" and "Flow".

![Start screen in Xiaomi Home App](/images/posts/xiaomi_bedsidelamp_2_review/screen_start.png){:class="img-fluid" width="300px" loading="lazy"}

### White option

With the "white" option, you have the can select any white light (warm white, cold white or anything in between) by dragging your finger left or right. On the top of the screen, you can see the color temperature and the brightness.

![White color screen in Xiaomi Home App](/images/posts/xiaomi_bedsidelamp_2_review/screen_white.png){:class="img-fluid" width="300px" loading="lazy"}

### Color option

Next to the "white" option, you have the color option. With the color option you can select any color by dragging your finger left or right. On top of the screen, you can see which colors are available.

![Color screen in Xiaomi Home App](/images/posts/xiaomi_bedsidelamp_2_review/screen_color.png){:class="img-fluid" width="300px" loading="lazy"}

### Flow option

Xiaomi has saved the best, for last. The "flow" option, to be specific. The flow option sets the device in a mode where it "flows" between all colors in the RGB spectrum.

![Flow screen in Xiaomi Home App](/images/posts/xiaomi_bedsidelamp_2_review/screen_flow.png){:class="img-fluid" width="300px" loading="lazy"}

For all modes, you can drag up or down on the screen to change the brightness.

## On device control

There is also to control the Xiaomi Mijia Bedside Lamp 2 without evening touching the the app (or your phone, for that matter).

![Front of the Xiaomi Mijia Bedside Lamp 2](/images/posts/xiaomi_bedsidelamp_2_review/lamp_8.jpg){:class="img-fluid" width="300px" loading="lazy"}

It has a simple on/off touch control, if you touch it, the night lamp will turn on, on the previous brightness and color setting.

![Front of the Xiaomi Mijia Bedside Lamp 2, highlighted power button](/images/posts/xiaomi_bedsidelamp_2_review/lamp_8_powerbtn.jpg){:class="img-fluid" width="300px" loading="lazy"}

Above that, there is a toucher slider which allows you to set the brightness of the current color. Changing the brightness, is pretty smooth and stepless. 

![Front of the Xiaomi Mijia Bedside Lamp 2, brightness slider](/images/posts/xiaomi_bedsidelamp_2_review/lamp_8_slider.jpg){:class="img-fluid" width="300px" loading="lazy"}

Here is a little GIF, where you can see the slider in action 😊.

<figure class="gifplayer">
    <img src="/images/posts/xiaomi_bedsidelamp_2_review/brightness_control.jpg" alt="Preview of the Xiaomi brightness control" height="180" width="320" data-alt="/images/posts/xiaomi_bedsidelamp_2_review/brightness_control.gif" loading="lazy">
</figure>

Lastly, we have a touch control which looks like 2 arrows pointing at each other. This option allows you the switch colors based on your favorite colors. Those colors can be set in the Xiaomi Home App.   

![Front the Xiaomi Mijia Bedside Lamp 2, highlighted favourite colours switcher](/images/posts/xiaomi_bedsidelamp_2_review/lamp_8_colour_switcher.jpg){:class="img-fluid" width="300px" loading="lazy"}

## Colours

The 3 primary colours on the Xiaomi Mijia Bedside Lamp 2, looks awesome! The colours look really vibrant, deep and totally not like any cheap LED's. I have no question about the rated CRI (Color Rendering Index) of Ra80, without 

![Xiaomi Mijia Bedside Lamp 2 showing red](/images/posts/xiaomi_bedsidelamp_2_review/lamp_color_1.jpg){:class="img-fluid" width="300px" loading="lazy"}

![Xiaomi Mijia Bedside Lamp 2 showing green](/images/posts/xiaomi_bedsidelamp_2_review/lamp_color_2.jpg){:class="img-fluid" width="300px" loading="lazy"}

![Xiaomi Mijia Bedside Lamp 2 showing blue](/images/posts/xiaomi_bedsidelamp_2_review/lamp_color_3.jpg){:class="img-fluid" width="300px" loading="lazy"}

Even the more harder colours, like purple / pink-ish actually looks good. The two Xiaomi's show no colour difference between them. As you can see in the picture below.

![Xiaomi Mijia Bedside Lamp 2 showing purple](/images/posts/xiaomi_bedsidelamp_2_review/lamp_color_4.jpg){:class="img-fluid" width="300px" loading="lazy"}

## Conclusion

All in all, it's looks and feels fanastic! I have personally been using the two unit for about 4 months now and I don't have any complaints. It works reliable, through the app as well as physical.

What I particularly like about the nightlight is that, the minimum brightness is 2 lumen. Which is especially nice, when home into the bedroom late in the evening and don't want to the whole room to be lit or when there is already another person sleeping.

In an upcoming blogpost, I will go over setting up the Xiaomi Mijia Bedside Lamp 2 in Home Assistant. And what potential automations you can setup.

### Where to buy

If you would like to buy the Xiaomi Mijia Bedside lamp, you can do so at the following retailers:

- <a href="https://www.amazon.de/gp/product/B07SPH2Y3X/ref=as_li_tl?ie=UTF8&camp=247&creative=1211&creativeASIN=B07SPH2Y3X&linkCode=as2&tag=sandervankast-21&linkId=998085a59a368f4c02684b1fd45c1c1e" target="blank" data-proofer-ignore>Amazon 🇩🇪</a>
- <a href="https://www.amazon.nl/gp/product/B07SPH2Y3X/ref=as_li_tl?ie=UTF8&camp=247&creative=1211&creativeASIN=B07SPH2Y3X&linkCode=as2&tag=sandervankast-21&linkId=7b33355b36a1a6f8375da9b0d6a3b3d1" target="blank" data-proofer-ignore>Amazon 🇳🇱</a>
- <a href="https://www.amazon.co.uk/gp/product/B07SPH2Y3X/ref=as_li_tl?ie=UTF8&camp=247&creative=1211&creativeASIN=B07SPH2Y3X&linkCode=as2&tag=sandervankast-21&linkId=998085a59a368f4c02684b1fd45c1c1e" target="blank" data-proofer-ignore>Amazon 🇬🇧</a>
- <a href="https://www.amazon.com/gp/product/B07SPH2Y3X/ref=as_li_tl?ie=UTF8&camp=247&creative=1211&creativeASIN=B07SPH2Y3X&linkCode=as2&tag=sandervankast-21&linkId=998085a59a368f4c02684b1fd45c1c1e" target="blank" data-proofer-ignore>Amazon 🇺🇸</a>
<!-- - [Aliexpress](https://httpbin.org/status/404){:target="blank"} -->
- [Banggood](https://www.banggood.com/custlink/G3KYCG6gtV){:target="blank"}
- [Bol.com](https://partner.bol.com/click/click?p=2&t=url&s=1144675&f=TXL&url=https%3A%2F%2Fwww.bol.com%2Fnl%2Fp%2Foriginele-xiaomi-mijia-bed-lamp-2-led-nacht-lichte-aanraking-slimme-app-bediening%2F9200000110917907%2F&name=Originele%20Xiaomi%20Mijia%20bed%20Lamp%202%20LED%20nacht%20li...&subid=xiaomi_mijia_bedsidelamp_review){:target="_blank"}

### Disclaimer

I have not been paid or sponsored in any way, to write this review and I am writing out of my own motivation. This review however, may contain affiliate links. Such links will reward me a small commission - at no extra cost for you.