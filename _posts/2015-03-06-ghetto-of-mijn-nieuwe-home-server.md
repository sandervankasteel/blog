---
layout: post
status: publish
published: true
title: 'Ghetto of mijn nieuwe home-server'
author: Sander van Kasteel
date: '2015-03-06 22:22:58 +0100'
categories:
- Hardware
- Server
tags: []
comments: []
---
<p>Eigenlijk was ik al een tijd toe aan een nieuwe home server want mijn&nbsp;Raspberry Pi was echt niet krachtig genoeg meer voor alle taken die ik hem toe bedeelde, dus was het tijd voor wat vervanging.</p>
<p>Laatst&nbsp;was ik even op visite bij&nbsp;wat familie en die hadden nog een Packard Bell iMedia S2110 machine staan die niet meer gebruikt werd. Leuk!</p>
<p><!--more--></p>

<p><a href="/images/posts/600x600.jpg"><img class="size-medium wp-image-162 alignnone img-fluid" src="/images/posts/600x600-300x300.jpg" alt="Packard Bell iMedia S2110 case"/></a></p>
<p>Packard Bell maakt met de iMedia serie wat ze zelf "lifestyle PC's" noemen. Over het algemeen zijn dit machine's die gebouwd rondom de AMD E en Intel Atom / Pentium processoren worden opgebouwd en het liefste zo klein mogelijk. Denk hierbij aan&nbsp;Micro-ATX / Mini-ITX moederborden en vooral low-power.</p>
<p>Leuke hardware voor een simpele low-power home server in plaats van hardware voor een desktop waar Windows 7 of 8 op moet draaien.&nbsp;Als home-server ga ik hem ook inrichten.</p>
<p>Omdat de hardware&nbsp;nu ongeveer 3 jaar oud is, heeft de case al wat "mishandeling" doorgemaakt, dus heb ik er voor gekozen om alleen het moederbord, RAM&nbsp;en de voeding te gebruiken uit deze machine. De benodigde opslag capaciteit wordt geleverd door een 2,5 inch SATA harde schijf van Seagate die ik toevallig nog had liggen.</p>
<p><a href="/images/posts/IMG_7557.jpg"><img class="alignnone wp-image-164 size-full img-fluid" src="/images/posts/IMG_7557.jpg" alt="IMG_7557"/></a></p>
<p>Als behuizing&nbsp;ga ik een&nbsp;Coolermaster Elite 430 gebruiken want die had ik nog staan. Het enige nadeel aan deze kast is dat hij geen ondersteuning heeft voor 2.5" harde schijven, dus daarvoor heb ik een 2,5" naar 3,5" bracket gebruikt.</p>
<p><a href="/images/posts/IMG_7446.jpg"><img class="alignnone wp-image-163 size-full img-fluid" src="/images/posts/IMG_7446.jpg" alt="IMG_7446"/></a></p>
<p><a href="/images/posts/IMG_7559.jpg"><img class="alignnone wp-image-165 size-full img-fluid" src="/images/posts/IMG_7559.jpg" alt="IMG_7559"/></a></p>
<p>De hardware specs zijn alsvolgt;</p>
<ul>
<li>AMD E2-1800</li>
<li>4 Gb RAM</li>
<li>ATI Radeon HD 7340</li>
<li>160 Gb Seagate HDD</li>
</ul>
<p>Het moederbord is oorspronkelijk gemaakt door Acer en heeft als typenummer D1F-AD meegekregen bij zijn geboorte.</p>
<p><a href="/images/posts/IMG_7562.jpg"><img class="alignnone size-full wp-image-166 img-fluid" src="/images/posts/IMG_7562.jpg" alt="IMG_7562"/></a></p>
<p>De ATI Radeon GPU zit ingebakken bij de processor, dus misschien dat ik daar in de toekomst ook nog wat leuks mee doen.</p>
<p>Het is de bedoeling dat&nbsp;de server de volgende taken gaat afhandelen;</p>
<ul>
<li>HTTP server</li>
<li>PHP</li>
<li>MySQL</li>
<li>OpenVPN</li>
<li>En misschien&nbsp;MongoDB of een andere NoSQL database</li>
</ul>
<p>Omdat het een normaal&nbsp;Micro-ATX moederbord is, past die prima in de Coolermaster kast.</p>
<p><a href="/images/posts/IMG_7565.jpg"><img class="alignnone size-full wp-image-167 img-fluid" src="/images/posts/IMG_7565.jpg" alt="IMG_7565"/></a></p>
<p>Het enige wat er niet helemaal in paste is de voeding, dus dat probleem heb ik met wat&nbsp;tieraps moeten oplossen.&nbsp;Dit komt omdat de 24 pins ATX ongeveer een lengte heeft&nbsp;van 15 cm, dus had ik op dit moment geen andere optie dan de voeding dicht bij de 24 pins ATX te plaatsen.</p>
<p><a href="/images/posts/IMG_7572.jpg"><img class="alignnone size-full wp-image-168 img-fluid" src="/images/posts/IMG_7572.jpg" alt="IMG_7572"/></a></p>
<p>En omdat de voeding op zo'n "ghetto" manier vast gemaakt was, heb ik wel even netjes het stroomsnoer&nbsp;vast gemaakt met tieraps</p>

<a href="/images/posts/IMG_7577.jpg"><img class="alignnone size-full wp-image-170 img-fluid" src="/images/posts/IMG_7577.jpg" alt="IMG_7577"/></a> 

<a href="/images/posts/IMG_7575.jpg"><img class="alignnone size-full wp-image-169 img-fluid" src="/images/posts/IMG_7575.jpg" alt="IMG_7575"/></a>

<p>Want ik zou niet willen dat als er aan het stroomsnoer getrokken word, dat gelijk de voeding mee getrokken word. Met al gevolgen van dien.</p>
<p>Zoals je waarschijnlijk wel gemerkt hebt, is dit een redelijke "ghetto" job aan het worden. Vandaar dat ik deze server ook de naam "ghetto" heb meegegeven.</p>
<p>En na het bouwen mag dan eindelijk de installatie beginnen van "ghetto". Alleen deze keer heb ik er een time-lapse video van gemaakt.</p>

<iframe src="https://www.youtube.com/embed/1ELehHst-Gc" frameborder="0" allowfullscreen="allowfullscreen" height="315"></iframe>

<p>Ik heb op dit moment Debian Jessie met NGINX, PHP 5.5 en MySQL 5.5 ge&iuml;nstalleerd en het is de bedoeling dat "ghetto" buiten mijn PHP development server ook nog eens de backend gaat worden voor mijn toekomstige domotica project(en).</p>
<p>Ik heb gelijk even heel simpel backup shell scriptje geschreven voor het maken van backups. Het shell script word door cron&nbsp;elke nacht om 12 uur uitgevoerd. Het maakt een directory aan met de huidige datum volgens de ISO 8601 standaard, dus op 12 maart 2015 word er de volgende map aangemaakt "2015-03-12" en daarin komt 1 tar.bz2 bestand te staan met de naam "backup.2015-03-12.tar.bz2" waar de volledige harde schijf in gebackupt word met uitzondering van de directories /dev, /run, /media, /mnt, /sys en /proc want deze mappen worden dynamisch bij boot aangemaakt en gevuld. Dus heeft het niet zo heel erg veel nut om deze mee te backuppen. Maar voor dat deze backup file word aangemaakt word eerst "apt-get clean &amp;&amp; apt-get autoclean" uitgevoerd om te zorgen dat de belachelijke hoeveelheid aan cache van APT / DPKG niet mee gebackupd word. Vervolgens word deze&nbsp;backup file geplaatst in /mnt/backup en in mijn geval is /mnt/backup via CIFS (aka SMB) gemount aan een directory op mijn NAS. De source van mijn backup script&nbsp;is hieronder te vinden.</p>

```bash
#!/bin/sh
# Written by Sander van Kasteel
# Copyright 2015
# LICENSE: GNU/GPL version 2
DATE=$(date +"%F")
FILENAME="backup.$DATE.tar.bz2"
# Clean APT cache
apt-get clean &amp;&amp; apt-get autoclean<

mkdir /mnt/backup/$(date +"%F")

echo "Backing-up data, please wait"
tar cfj /mnt/backup/$DATE/$FILENAME / --exclude="/sys" --exclude="/tmp" --exclude="/dev" --exclude="/mnt" --exclude="/proc" --exclude="/media" --exclude="/run"
```

<p>Het enige wat ik nog wil veranderen aan mijn backup script is dat die automatisch backups naar een "x" aantal dagen verwijderd. Maar dat is iets voor later, zodra ik genoeg backups gemaakt heb ;)</p>
