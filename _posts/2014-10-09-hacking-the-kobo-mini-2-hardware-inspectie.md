---
layout: post
status: publish
published: true
title: 'Hacking a Kobo Mini - Hardware inspectie'
page_title: 'Hacking a Kobo Mini #2 - Hardware inspectie'
author: Sander van Kasteel
date: '2014-10-09 19:09:25 +0200'
categories:
- Hacking
tags:
- kobo
- kobo mini
- hacking
- linux
---
<p>Deze post is als vervolg op de vorige post&nbsp;<a title="Deel 1" href="/hacking/2014/09/25/hacking-the-kobo-mini.html" target="_blank">https://www.sandervankasteel.nl/hacking/2014/09/25/hacking-the-kobo-mini.html</a>&nbsp;bedoeld.</p>
<p>Voor dat ik &uuml;berhaupt iets kan doen, moet ik als eerst maar eens zorgen dat de Kobo uitgeschakeld is. Dat kunnen we doen door de sleep/wake knop lang genoeg naar rechts te duwen. Dan krijg je als het goed is vanzelf de melding "Uitgeschakeld". Zoals je hier onder kan zien.</p>
<p><a href="/images/posts/DSC_3033.jpg"><img class="alignnone size-large wp-image-54" src="/images/posts/DSC_3033-1024x685.jpg" alt="Kobo Mini uitgeschakeld" width="584" height="390" /></a></p>
<p>Zodra die eenmaal uitgeschakeld is, kunnen we de Kobo Mini openmaken door allereerst de "verwisselbare achterkant los te halen" en heb ik de 6 schroeven die het apparaat mij laat zien kunnen verwijderen. Gelukkig zijn het 6 dezelfde schroeven, dus hoef je niet op te letten welke nou precies waar hoort.<!--more--></p>
<p><a href="/images/posts/DSC_3037.jpg"><img class="alignnone size-large wp-image-55" src="/images/posts/DSC_3037-1024x685.jpg" alt="Kobo achterkant" width="584" height="390" /></a></p>
<p>Na dat alle 6 de schroeven verwijderd hebt, kan je zonder al te veel moeite de Kobo Mini&nbsp;open maken en toen "In like Flynn". De Kobo Mini geeft&nbsp;al zijn hardwarematige geheimen prijs.</p>
<p><a href="/images/posts/DSC_3054.jpg"><img class="alignnone wp-image-61 size-large" src="/images/posts/DSC_3054-1024x828.jpg" alt="DSC_3054" width="584" height="472" /></a></p>
<p>BAZINGA! Een 2 Gb MicroSD kaart, zoals ik ook al vermoede. Mooi, dat schept mogelijkheden. Maar laten we voor de rest eens kijken wat we nog meer zien. Een standaard 3.7v 1000 mAh accu, een Samsung K4X2G323PC, een Neonode zForce&nbsp;NN1001 en een Freescale&nbsp;MCIMX507CVM8B. De zForce NN1001 blijkt na wat onderzoek een touchscreen controller te zijn [1]. De Samsung&nbsp;K4X2G323PC is een stukje DDR RAM [2]. Maar wat veel belangrijker is, is de Freescale&nbsp;MCIMX507CVM8B [3].</p>
<p>De Kobo Mini blijkt tot mijn grote verbazing best wel een leuke processor te hebben en tot veel meer mogelijk dan waar die op dit moment gebruikt word. Het is een 800 Mhz singlecore ARM processor gebouwd op de Cortex-A8 architectuur, daarnaast heeft de processor de mogelijk om te USB poort om te zetten naar een USB-OTG poort (USB On The Go) waardoor de Kobo Mini gaat dienst doen als USB-host ipv USB-slave.<br />
Daarnaast heeft de Kobo Mini volgens mij de mogelijkheid tot een seriele terminal (zie links van de SD kaart slot) maar dan moet je er wel de juiste headerpins op solderen.</p>
<p>Al met al leuk speelgoed dus! :)</p>
<p>Maar voor dat we &uuml;berhaupt eens wat kunnen gaan doen met die software. Laten we eerst eens kijken wat met dat SD kaart slot kunnen doen. Eerst maar eens even een dump maken van de huidige 2Gb microSD kaart. Dit heb ik even heel simpel gedaan met "dd".</p>
<blockquote><p>[sander@titan kobo]$ sudo dd if=/dev/sdc of=kobo-sd.img bs=1M<br />
[sudo] password for sander:<br />
1886+0 records in<br />
1886+0 records out<br />
1977614336 bytes (2.0 GB) copied, 137.743 s, 14.4 MB/s</p></blockquote>
<p>Deze image heb ik toen gelijk weer terug geschreven naar een 4Gb MicroSD kaart.</p>
<blockquote><p>[sander@titan kobo]$ sudo dd if=kobo-sd.img of=/dev/sdc bs=1M<br />
1886+0 records in<br />
1886+0 records out<br />
1977614336 bytes (2.0 GB) copied, 420.13 s, 4.7 MB/s</p></blockquote>
<p>Theoretisch zou dit betekenen dat ik nu een 4Gb Kobo Mini heb ipv een 2Gb model. Maar voor dat we dat kunnen realiseren zal ik waarschijnlijk wel eerst even de partities op de microSD kaart moeten vergroten. Want dit is de structuur van de SD kaart voor dat ik de partities heb vergroot.</p>
<p><a href="/images/posts/partition-before.png"><img class="alignnone size-large wp-image-69" src="/images/posts/partition-before-1024x623.png" alt="partition-before" width="584" height="355" /></a></p>
<p>En zo ziet het&nbsp;er uit na dat ik de "data" partitie (waar al je eBooks op staan) heb vergroot.</p>
<p><a href="/images/posts/partition-after.png"><img class="alignnone size-large wp-image-71" src="/images/posts/partition-after-1024x623.png" alt="partition-after" width="584" height="355" /></a></p>
<p>Zoals je kan zien hebben we 3 partities;</p>
<ol>
<li>rootfs</li>
<li>recoveryfs</li>
<li>KOBOeReader</li>
</ol>
<p>Mijn vermoeden is als volgt, op de "rootfs" partitie leeft het OS met alle userland tools. De "recoveryfs" partitie is zoals de naam ook al impliceert, een recovery image en word waarschijnlijk gebruikt op het moment dat als het apparaat in een recovery modus gezet word om het OS op terug te zetten of als de gebruiker kiest voor "fabrieks instellingen herstellen". Ik vermoed daarnaast dat de partitie KOBOeReader gebruikt word om alle eBooks op te slaan gezien het gekozen formattering (FAT32) en dat de partitie een behoorlijk stuk groter is dan de rest.</p>
<p>Nou tijd om mijn 4Gb microSD kaartje er in te zetten en kijken wat die doet...</p>
<p><a href="/images/posts/DSC_3060.jpg"><img class="alignnone size-large wp-image-73" src="/images/posts/DSC_3060-1024x685.jpg" alt="DSC_3060" width="584" height="390" /></a></p>
<p>BINGO! First try, first hit! Het werkt! De Kobo start netjes op en herkend zoals je hier onder kan zien, zonder problemen de 2Gb extra aan ruimte. Dat is mooi.</p>
<p><a href="/images/posts/DSC_3061.jpg"><img class="alignnone size-large wp-image-75" src="/images/posts/DSC_3061-1024x685.jpg" alt="DSC_3061" width="584" height="390" /></a></p>
<p>Dat bekend theoretisch dat je ieder formaat SDHC kaart er in de Kobo Mini kan gebruiken zonder problemen. In mijn volgende post ga ik de software die er op draait behandelen. Kijken wat daar mogelijk is en een begin maken met de eerste software modificaties. Wat ik als eerste draaiende wil hebben is een telnet of SSH server op de Kobo Mini. Want zodra ik remote kan inloggen op de Kobo mini (hetzij via SSH of telnet) kan ik op afstand dingen gaan doen zoals een debugger zodat ik kan inzien wat er allemaal gebeurt in de applicatie waarin je je eBooks leest&nbsp;want ik ben vooral benieuwd of ik dat proces nog andere applicaties aanroept of dat het allemaal 1 gigantische blob is...</p>
<p>[1]&nbsp;<a href="http://www.neonode.com/neonode%C2%AE-announces-the-world%E2%80%99s-first-single-chip-optical-touch-controller/" target="_blank" data-proofer-ignore>http://www.neonode.com/neonode%C2%AE-announces-the-world%E2%80%99s-first-single-chip-optical-touch-controller/</a><br />
[2]&nbsp;<a href="http://www.samsung.com/global/business/semiconductor/product/mobile-dram/detail?iaId=749&amp;productId=7613" target="_blank">http://www.samsung.com/global/business/semiconductor/product/mobile-dram/detail?iaId=749&amp;productId=7613<br />
</a>[3]&nbsp;<a href="http://cache.freescale.com/files/32bit/doc/data_sheet/IMX50CEC.pdf" target="_blank">http://cache.freescale.com/files/32bit/doc/data_sheet/IMX50CEC.pdf</a></p>
