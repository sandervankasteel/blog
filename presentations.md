---
layout: page
title: Presentations
permalink: /presentations/
---

# April 2022 GroningenPHP

![GroningenPHP logo](/images/phpgroningen_header.jpg)

Security, is a big word for an even bigger concept. But what do we actually mean by that?
And how can we as developers (and maintainers of applications) make our code better and defend ourselves and the applications we build, against bad actors. I will be talking about the most common attacks you can come across in your application(s), explain how those attacks work, and potential solutions against such attacks


[Presentation 🗒](https://docs.google.com/presentation/d/1ynf3uVy6xX4FzPXcj5Y20sMXOK2W__mBk6Qa2yL2h9Y/edit?usp=sharing)


<br />

# July 2020 GroningenPHP

![GroningenPHP logo](/images/phpgroningen_header.jpg)

You can find the code and the presentation I gave on the 2nd of July 2020 for the [GroningenPHP meetup](https://www.meetup.com/GroningenPHP/events/jhvhqrybckbdb/) on "Laravel 7 by example".


## Links

[Code 👨‍💻](https://github.com/sandervankasteel/l7-demo-phpgrunn)

[Presentation 🗒](https://docs.google.com/presentation/d/1puwsvbJbErP0lIc71BZ0qvPRgplj4Q16gTTHzEwWgyo/edit?usp=sharing)
